package com.example.attendancestudent.repository.firebase;

import com.google.firebase.auth.FirebaseAuth;

public class FirebaseAuthSingleton {
    private static FirebaseAuthSingleton instance;
    private FirebaseAuth mAuth;
    private FirebaseAuthSingleton(){
        mAuth = FirebaseAuth.getInstance();
    }
    public FirebaseAuth getFirebaseAuth(){
        return mAuth;
    }
    public static FirebaseAuthSingleton getInstance(){
        if(instance==null){
            instance = new FirebaseAuthSingleton();
        }
        return instance;
    }
}
