package com.example.attendancestudent.repository.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.attendancestudent.listenerinterface.DataLoadListener;
import com.example.attendancestudent.model.Class;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseDAO {
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private FirebaseAuth mAuth;
    private String currentIDUser;
    static FirebaseDAO instance;
//    private DataLoadListener listener;
    private static String TAG = FirebaseDAO.class.getSimpleName();
    public static FirebaseDAO getInstance(){
        if(instance==null){
            instance = new FirebaseDAO();
        }
        return instance;
    }
//    static Context mContext;
//    public void initContext(Context context){
//        mContext = context;
//        listener = (DataLoadListener)mContext;
//    }
    public FirebaseDAO() {
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();

        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            //user is signed in
            this.currentIDUser = user.getUid();

        }
    }
    public boolean onLoggin(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        return user != null;
    }
    public void login(String email,String password,OnLogginListener listener){
        FirebaseAuth mAuth = FirebaseAuthSingleton.getInstance().getFirebaseAuth();
        Log.e("LOGIN","SUCCESS LOGIN inin inin 22222");
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.e("LOGIN","SUCCESS LOGIN inin inin ");
                if(!task.isSuccessful()){
                    Log.e("LOGIN","FAIL LOGINL "+task.getException());
                    listener.onFalied();
                }
                else{
                    Log.e("LOGIN","SUCCESS LOGIN ");
                    listener.onSuccessful();
                }
            }
        });
    }

    public void addAttendance(String keyClass,String IDStu){
        String IDSubj="test";
        String date = "145465465";
        String reason = "sss";
        String status = "false";

//        mFirebaseDatabase.child("Attendances").push().setValue(new Attendance(IDStu,"subj1","-M3Bch5OzWgNLvrCMIlo",date,reason,status));
    }


    public boolean renameClass(String key, String name) {

        Task task = mFirebaseDatabase.child("Classes").child(key).child("nameOfClass").setValue(name);
        return task.isSuccessful();
    }


    public void deleteClass(String key) {//phai delete cac hoc sinh cua class do
        mFirebaseDatabase.keepSynced(true);

        mFirebaseDatabase.child("Person").child("Students").orderByChild("keyClass").equalTo(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    child.getRef().removeValue();
                }
                Task task = mFirebaseDatabase.child("Classes").child(key).removeValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    public void getAllClasses(String keyTeacher,DataLoadListener listener) {
        List<Class> listClass = new ArrayList<>();
        mFirebaseDatabase.keepSynced(true);
        mFirebaseDatabase.child("Classes").orderByChild("idTea").equalTo(keyTeacher).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Class classChild = child.getValue(Class.class);
                    classChild.setKeyClass(child.getKey());
                    listClass.add(classChild);
                }
                listener.onListLoaded(listClass);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
//        return listClass;
    }
    public void getAllClassesRealTime(String keyTeacher,DataLoadListener listener) {
        List<Class> listClass = new ArrayList<>();
        mFirebaseDatabase.keepSynced(true);
        mFirebaseDatabase.child("Classes").orderByChild("idTea").equalTo(keyTeacher).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Class classChild = child.getValue(Class.class);
                    classChild.setKeyClass(child.getKey());
                    listClass.add(classChild);
                }
                listener.onListLoaded(listClass);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }



    public void getKeyTeacher(OnKeyTeacherListener listener) {
//        Log.d("CURRUSERID", currentIDUser);
        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null){
            mFirebaseDatabase.child("Person").child("Teachers").orderByChild("iduser").equalTo(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    System.out.println("INNNNNNNNNNNN");
                    String keyTeacher = "";
                    for(DataSnapshot child:dataSnapshot.getChildren()){
                        keyTeacher = child.getKey();
                    }
                    listener.onReceiveKey(keyTeacher);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

    }



    public void addClass(Class newClass) {
        mFirebaseDatabase.keepSynced(true);
        // Em phải lưu 1 biến class trong cái DAO này
        String keyClass = mFirebaseDatabase.child("Classes").push().getKey();
        Class temp = newClass;
        temp.setKeyClass(keyClass);
        System.out.println("KEST"+keyClass);
        mFirebaseDatabase.child("Classes").child(keyClass).setValue(temp);
    }

    public interface OnKeyTeacherListener{
        void onReceiveKey(String keyTeacher);
    }
    public interface OnLogginListener{
        void onSuccessful();
        void onFalied();
    }



}
