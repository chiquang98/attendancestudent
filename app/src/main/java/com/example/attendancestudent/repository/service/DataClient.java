package com.example.attendancestudent.repository.service;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface DataClient {
    @FormUrlEncoded
    @POST("webservice.php")
    Call<String> InsertData(@Field("data") String name);

    @Multipart
    @POST("upload")
    Call<String> predictStudent(@Part MultipartBody.Part photo);

    //du lieu tra ve la mot list ten/ma sinh vien detect duoc tu anh gui len
    @Multipart
    @POST("attendance")//truong hôp nay chỉ gửi 1 ảnh để điểm danh
    Call<List<String>> attend(@Part MultipartBody.Part photoOfClass);


    @Multipart
    @POST("uploaddatatrain")
    Call<String> uploadPhotos(@Part List<MultipartBody.Part> files,@Part("msv") RequestBody studentCode);
}
