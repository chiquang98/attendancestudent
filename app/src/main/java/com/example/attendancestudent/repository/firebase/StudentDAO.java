package com.example.attendancestudent.repository.firebase;
import android.util.Log;
import androidx.annotation.NonNull;
import com.example.attendancestudent.model.Attendance;
import com.example.attendancestudent.model.Student;
import com.example.attendancestudent.model.User;
import com.example.attendancestudent.tableview.model.RowHeader;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private static StudentDAO instance;
    public static StudentDAO getInstance(){
        if(instance==null){
            instance = new StudentDAO();
        }
        return instance;
    }

    public StudentDAO() {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();
    }

    public void getAllStudent(String keyClass, @NonNull OnStudentListener listener) {

        List<Student> listStu = new ArrayList<>();
        mFirebaseDatabase.child("Person").child("Students").orderByChild("keyClass").equalTo(keyClass).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null){
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        Student student = child.getValue(Student.class);
                        student.setKeyStudent(child.getKey());
//                    RowHeader header = new RowHeader(student.getMsv(), student.getFullName());
                        listStu.add(student);
                    }
                    listener.onReceiveData(listStu);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public interface OnExsitedStudentListener{
        void onExsitedListener(boolean isExsited);
    }
    public void isStutentExsisted(String keyStudent,OnExsitedStudentListener onExsitedStudentListener){
        mFirebaseDatabase.child("Person").child("Students").orderByChild("msv").equalTo(keyStudent).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){//neu ton tai
                    onExsitedStudentListener.onExsitedListener(true);
                }
                else{
                    onExsitedStudentListener.onExsitedListener(false);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void addStudent(Student student) {
        mFirebaseDatabase.child("Person").child("Students").push().setValue(student);
    }
    public void addStudents(List<Student> students){
        //delete cai cu da
//        String key = students.get(0).getKeyClass();
//        mFirebaseDatabase.child("Person").child("Students").orderByChild("keyClass").equalTo(key).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for(DataSnapshot child:dataSnapshot.getChildren()){
//                    child.getRef().removeValue();
//                }
////                Task task = mFirebaseDatabase.child("Classes").child(key).removeValue();
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        for(Student student:students){
            mFirebaseDatabase.child("Person").child("Students").push().setValue(student);
        }

    }

    public interface OnStudentListener {
        void onReceiveData(List<Student> list);
    }
}
