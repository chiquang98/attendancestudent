package com.example.attendancestudent.repository.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.attendancestudent.model.Attendance;
import com.example.attendancestudent.model.Student;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AttendanceDAO {
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private static AttendanceDAO instance;
    public static AttendanceDAO getInstance(){
        if(instance==null){
            instance = new AttendanceDAO();
        }
        return instance;
    }

    public AttendanceDAO() {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();
    }
    public void addAttendance(List<Attendance> listAttendance){
        for(int i=0;i<listAttendance.size();i++){
            String keyAttend = mFirebaseDatabase.child("Attendances").push().getKey();
            Attendance attendance = listAttendance.get(i);
            attendance.setIDAttend(keyAttend);
            mFirebaseDatabase.child("Attendances").child(keyAttend).setValue(attendance);
        }
    }
    public void getDataAttenance(String keyClass, OnAttendanceListener listener){
        List<Attendance> attendanceList = new ArrayList<>();
        mFirebaseDatabase.child("Attendances").orderByChild("idclass").equalTo(keyClass).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot child:dataSnapshot.getChildren()){

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public interface OnAttendanceListener{
        void onReceiveData(List<Attendance> listAttend);
    }

}
