package com.example.attendancestudent.repository.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.attendancestudent.model.Event;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class EventDAO {
    private static EventDAO instance = null;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    public EventDAO(){
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();
    }
    public static EventDAO getInstance() {
        if(instance==null){
            instance = new EventDAO();
        }
        return instance;
    }
    public void addEvent(Event event){
        mFirebaseDatabase.keepSynced(true);
        mFirebaseDatabase.child("Timetable").push().setValue(event);
    }
    public void deleteEventByKeyEvent(String keyEvent){
        mFirebaseDatabase.keepSynced(true);
        Task task = mFirebaseDatabase.child("Timetable").child(keyEvent).removeValue();
    }
    public void getAllEvent(String keyTeacher,OnEventListener onEventListener){
        List<Event> list = new ArrayList<>();
        mFirebaseDatabase.keepSynced(true);
        mFirebaseDatabase.child("Timetable").orderByChild("keyTeacher").equalTo(keyTeacher).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e("CHECKLOADEVENT",dataSnapshot.toString());
                for(DataSnapshot child: dataSnapshot.getChildren()){
                    Log.e("CHECKLOADEVENT",child.getValue().toString());
                    Event eventChild = child.getValue(Event.class);
                    eventChild.setKeyEvent(child.getKey());
                    list.add(eventChild);
                }
                onEventListener.onEventChange(list);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("CHECKLOADEVENT",databaseError.getMessage());
            }
        });
    }
    public interface OnEventListener{
        void onEventChange(List<Event> eventList);
    }
}
