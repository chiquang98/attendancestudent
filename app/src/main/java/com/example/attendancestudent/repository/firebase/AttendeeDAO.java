package com.example.attendancestudent.repository.firebase;

import androidx.annotation.NonNull;

import com.example.attendancestudent.model.Attendee;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AttendeeDAO {
    private static AttendeeDAO instance;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    public AttendeeDAO() {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();
    }

    public static AttendeeDAO getInstance() {
        if (instance == null) {
            instance = new AttendeeDAO();
        }
        return instance;
    }

    public void getAttendeeByClass(String keyClass, OnAttendeesListener onAttendeesListener) {
        //danh sach tra ve cac ngay, va ngay do danh sach gom nhung ai di
        List<Map<String, List<Attendee>>> listAttendee;
        mFirebaseDatabase.child("classAttendees").child(keyClass).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, List<Attendee>> attendeesAllDays = new HashMap<>();
                GenericTypeIndicator<List<Attendee>> t = new GenericTypeIndicator<List<Attendee>>() {
                };
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    List<Attendee> attendees = child.getValue(t);
                    attendeesAllDays.put(child.getKey(), attendees);//1 ngay
                }
                onAttendeesListener.onReceiveAttendees(attendeesAllDays);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void attend(List<Attendee> attendees, String day, String keyClass) {
//            Map<String,List<Attendee>> dayAtendees = new HashMap<>();
//            dayAtendees.put(day,attendees);
        mFirebaseDatabase.child("classAttendees").child(keyClass).child(day).setValue(attendees);
    }
    public void addStutoAttendTable(Attendee attendee, List<String> listDays, String keyClass,int numberOfStu) {
//            Map<String,List<Attendee>> dayAtendees = new HashMap<>();
//            dayAtendees.put(day,attendees);
        for(String day:listDays){
            mFirebaseDatabase.child("classAttendees").child(keyClass).child(day).child(Integer.toString(numberOfStu)).setValue(attendee);
        }

    }
    public interface OnAttendeesListener {
        void onReceiveAttendees(Map<String, List<Attendee>> maps);
    }

}
