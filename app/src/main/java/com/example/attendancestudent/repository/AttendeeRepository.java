package com.example.attendancestudent.repository;

import android.util.Log;

import com.example.attendancestudent.repository.service.APIUtils;
import com.example.attendancestudent.repository.service.DataClient;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendeeRepository {
    private static volatile AttendeeRepository instance;//bien volatile co tac dung thong bao su thay doi toi cac thread khac neu co
    private DataClient dataClient = APIUtils.getData();
    private String TAG = AttendeeRepository.class.getSimpleName();
    public AttendeeRepository() {
    }
    public static AttendeeRepository getInstance(){
        if(instance==null){
            return new AttendeeRepository();
        }
        return instance;
    }
    public void attendance(String pathImage,onAttendanceListener onAttendanceListener){
        File file = new File(pathImage);
        String file_path = file.getAbsolutePath();
        String[] mangtenfile = file_path.split("\\.");
//                file_path = mangtenfile[0] + System.currentTimeMillis() +'.'+ mangtenfile[1];
        file_path = System.currentTimeMillis() + "." + mangtenfile[1];
        Log.d(TAG,"path: "+pathImage);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("file",file_path,requestBody);
        Call<List<String>>callBack = dataClient.attend(multipartBody);
        callBack.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                Log.d(TAG,"List Stu Size: "+response.toString());
                onAttendanceListener.onAttendance(response.body());
                Log.d(TAG,"List Stu Size: "+response.body());
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                Log.d(TAG,"FAIL: "+t.getMessage());
            }
        });
    }

    public interface onAttendanceListener{
        void onAttendance(List<String> listAttendee);
    }
}
