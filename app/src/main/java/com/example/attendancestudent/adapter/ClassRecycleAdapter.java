package com.example.attendancestudent.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.view.menu.MenuBuilder;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendancestudent.R;
import com.example.attendancestudent.listenerinterface.OnItemClickListener;
import com.example.attendancestudent.model.Class;
import com.example.attendancestudent.repository.firebase.FirebaseDAO;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ClassRecycleAdapter extends RecyclerView.Adapter<ClassRecycleAdapter.ViewHolder> implements Filterable {
    private List<Class> mClassList = new ArrayList<>();
    private List<Class> mClassListAll ;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private static final String TAG = ClassRecycleAdapter.class.getSimpleName();
    private OnItemClickListener onItemClickListener; // Global scope
    private OnClickMenuClassOptionListener onClickMenuClassOptionListener;

    public ClassRecycleAdapter(Context context, List<Class> list) {
//        if (list != null) mClassList.addAll(list);
        mClassList = list;
//       this.onItemClickListener =mOnItemClickListener;
        mContext = context;
//        mLayoutInflater = LayoutInflater.from(context);
            mClassListAll = new ArrayList<>(list);

    }

    public OnClickMenuClassOptionListener getOnClickMenuClassOptionListener() {
        return onClickMenuClassOptionListener;
    }

    public void setOnClickMenuClassOptionListener(OnClickMenuClassOptionListener onClickMenuClassOptionListener) {
        this.onClickMenuClassOptionListener = onClickMenuClassOptionListener;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    @Override
    public ClassRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.class_list_item_row, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ClassRecycleAdapter.ViewHolder holder, int position) {
        Class classModel = mClassList.get(position);

        holder.container.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_scale_animation));
        holder.tvClassName.setText("Lớp: "+classModel.getNameOfClass());
        holder.tvSubjectName.setText("Môn : "+classModel.getSubject());
//        holder.tvNumberOfStus.setText(25 + "");
//        holder.tvYear.setText("2020");
    }
    @Override
    public int getItemCount() {
//        System.out.println(mClassList.size());
        return mClassList.size();
    }

    public void setClasses(List<Class> listClass) {
            if(this.mClassList.size()==0)
            {
                this.mClassList = listClass;
                this.mClassListAll = listClass;
                Log.e("HomeFragment1","in SetClasses"+mClassListAll.size()+"");
                notifyDataSetChanged();
            }

    }
    @Override
    public Filter getFilter() {
        return filterObject;
    }
    private Filter filterObject = new Filter() {
        @Override
        //Run on Background thread
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Class> filteredList = new ArrayList<>();
            if(constraint==null||constraint.length()==0||constraint.toString().isEmpty()){
//                filteredList.addAll(mClassListAll);
                mClassList = mClassListAll;
            }
            else{
                String filterPattern = constraint.toString().toLowerCase();
                for(Class classItem:mClassListAll){
                    if(classItem.getNameOfClass().toLowerCase().contains(filterPattern)){
                        filteredList.add(classItem);
                    }
                }
                mClassList = filteredList;
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = mClassList;
            Log.e("HomeFragment1",mClassListAll.size()+"");
            return filterResults;
        }
        //run on UI thread
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
//            mClassList.clear();
            mClassList=((List)results.values);
            notifyDataSetChanged();
        }
    };
    public interface OnClickMenuClassOptionListener {
        void onClicked(int positionRow, int positionOption, String keyClass);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView moreIcon;
        private TextView tvClassName, tvNumberOfStus, tvYear,tvSubjectName;
        private RelativeLayout container;
        Dialog mDialog;
        private PopupMenu popupMenu;//for menu more option list view

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("hihihihihihihi");
                    if (onItemClickListener != null) {
                        int position = getAdapterPosition();
                        System.out.println("Position"+position);
                        if (position != RecyclerView.NO_POSITION) {
                            onItemClickListener.onItemClicked(position,mClassList.get(position).getKeyClass());
                        }
                    }

                }

            });
            container = itemView.findViewById(R.id.container_row);
            moreIcon = itemView.findViewById(R.id.more_icon);
            tvClassName = itemView.findViewById(R.id.m_class_name);
            tvSubjectName = itemView.findViewById(R.id.m_Subject_name);
//            tvNumberOfStus = (TextView) itemView.findViewById(R.id.m_number_of_students);
//            tvYear = (TextView) itemView.findViewById(R.id.m_year) ;
            moreIcon.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onClick(View v) {
                    System.out.println("ONMENUUUUUUUU");
                    popupMenu = new PopupMenu(mContext, v);

                    MenuInflater menuInflater = popupMenu.getMenuInflater();
                    menuInflater.inflate(R.menu.class_option_menu, popupMenu.getMenu());
                    try {
                        Method method = popupMenu.getMenu().getClass().getDeclaredMethod("setOptionalIconsVisible", boolean.class);
                        method.setAccessible(true);
                        method.invoke(popupMenu.getMenu(), true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    popupMenu.show();
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.class_item_delete:
                                    int pos = getAdapterPosition();
                                    System.out.println("position:" + pos);
                                    FirebaseDAO.getInstance().deleteClass(mClassList.get(pos).getKeyClass());
                                    mClassList.remove(pos);
                                    notifyItemRemoved(pos);

//                                    if (onClickMenuClassOptionListener != null) {
//
//                                        onClickMenuClassOptionListener.onClicked(getAdapterPosition(), 0,mClassList.get(getAdapterPosition()).getKeyClass());
//                                        //0: remove
////                                        mClassList.remove(getAdapterPosition());
////                                        System.out.println(getAdapterPosition());
////                                        mClassList.remove(getAdapterPosition());
////                                        notifyItemRemoved(getAdapterPosition());
//                                    }

//                                    mClassList.remove(getAdapterPosition());
                                    break;
                                case R.id.class_item_rename:
//                                    if (onClickMenuClassOptionListener != null) {
//                                        //1: rename
//                                        onClickMenuClassOptionListener.onClicked(getAdapterPosition(), 1,mClassList.get(getAdapterPosition()).getKeyClass());
//                                    }
                                    int posRename = getAdapterPosition();
                                    mDialog = new Dialog(mContext);
                                    mDialog.setContentView(R.layout.dialog_rename_class);
                                    final EditText dialog_tvName = mDialog.findViewById(R.id.edt_dialog_rename_nameClass);
                                    Button dialog_btnAdd = mDialog.findViewById(R.id.btn_dialog_rename_add);
                                    Button dialog_btnCancel = mDialog.findViewById(R.id.btn_dialog_rename_cancel);
                                    dialog_tvName.setText(mClassList.get(posRename).getNameOfClass());
                                    dialog_btnAdd.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Class temp = mClassList.get(posRename);
                                            String rename_Name = dialog_tvName.getText().toString();
//                                classViewModel.renameClass(positionRow,rename_Name);
                                            temp.setNameOfClass(rename_Name);
                                            FirebaseDAO.getInstance().renameClass(mClassList.get(posRename).getKeyClass(),rename_Name);
                                            mClassList.set(posRename, temp);
                                            notifyItemChanged(posRename);
                                            mDialog.cancel();
                                        }
                                    });
                                    dialog_btnCancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.cancel();
                                        }
                                    });
                                    mDialog.show();
                                    break;
                            }
                            return false;
                        }
                    });
                }
            });


            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(mContext, "Long item clicked " + tvClassName.getText(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        }

    }

}
