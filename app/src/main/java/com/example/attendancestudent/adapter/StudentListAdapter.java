package com.example.attendancestudent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.attendancestudent.R;
import com.example.attendancestudent.model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.ViewHolder> {
    private List<Student> mStudentList = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    public StudentListAdapter(Context context, List<Student> list) {
//        if (list != null) mClassList.addAll(list);
        mStudentList = list;
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNameStudent, tvID;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNameStudent = itemView.findViewById(R.id.m_student_name);
            tvID = itemView.findViewById(R.id.m_id_student);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, tvNameStudent.getText(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.student_list_item_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Student mStudent = mStudentList.get(position);
        holder.tvNameStudent.setText(mStudent.getFullName());
        holder.tvID.setText(mStudent.getMsv());
    }

    @Override
    public int getItemCount() {
        return mStudentList.size();
    }
}
