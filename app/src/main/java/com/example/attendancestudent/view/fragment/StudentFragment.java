package com.example.attendancestudent.view.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.OnScrollListener;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.filter.Filter;
import com.evrencoskun.tableview.pagination.Pagination;
import com.example.attendancestudent.R;
import com.example.attendancestudent.listenerinterface.DataLoadListener;
import com.example.attendancestudent.model.Attendee;
import com.example.attendancestudent.model.CellExcel;
import com.example.attendancestudent.model.Class;
import com.example.attendancestudent.model.Student;
import com.example.attendancestudent.repository.AttendeeRepository;
import com.example.attendancestudent.repository.firebase.AttendeeDAO;
import com.example.attendancestudent.repository.firebase.FirebaseDAO;
import com.example.attendancestudent.repository.firebase.StudentDAO;
import com.example.attendancestudent.tableview.TableViewAdapter;
import com.example.attendancestudent.tableview.TableViewListener;
import com.example.attendancestudent.tableview.TableViewModel;
import com.example.attendancestudent.tableview.model.Cell;
import com.example.attendancestudent.tableview.model.ColumnHeader;
import com.example.attendancestudent.tableview.model.RowHeader;
import com.example.attendancestudent.viewmodel.ClassViewModel;
import com.example.attendancestudent.viewmodel.StudentViewModel;
import com.github.ag.floatingactionmenu.OptionsFabLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = StudentFragment.class.getSimpleName();
    private static final int REQUEST_CODE_EXTERNAL_STORAGE = 3;
    private static final int REQUEST_OPEN_FILE_CHOOSER = 4;
    private final int REQUEST_CODE_IMAGE = 3;
    //

    TextView tvDate;
    Calendar calendar;
    TableViewModel tableViewModel;
    List<Student> listStu;
    List<String> days;
    List<Student> listStuVM = new ArrayList<>();
    String realPath = "";
    String actualfilepath = "";
    AttendeeDAO attendeeDAO;
    StudentDAO studentDAO;
    List<Class> spinnerClasses = new ArrayList<>();
    readFileStudentTask k;
    ProgressDialog progressDialog;
    private String keyClass = "";
    //    private FloatingActionButton mActionButton;
    private Button btnAttend, btnPickDate;
    private DatePickerDialog picker;
    private Spinner moodFilter, genderFilter;
    private ImageButton previousButton, nextButton;
    private TextView tablePaginationDetails;
    private TableView mTableView;
    private Filter mTableFilter; // This is used for filtering the table.
    private Pagination mPagination; // This is used for paginating the table.
    private OptionsFabLayout fabWithOptions;
    private Dialog mDialogAttendance;
    private Dialog mDialogAddStudent;
    private Dialog mDialogFetchTable;
    //true thi hien
    //false thi khong hien
    private boolean mPaginationEnabled = false;
    private int REQUEST_CAMERA = 1, SELECT_FILE = 0;
    private StudentViewModel studentViewModel;
    private ClassViewModel classViewModel;
    //
    // Handler for the changing of pages in the paginated TableView.
    @NonNull
    private Pagination.OnTableViewPageTurnedListener onTableViewPageTurnedListener = new
            Pagination.OnTableViewPageTurnedListener() {
                @Override
                public void onPageTurned(int numItems, int itemsStart, int itemsEnd) {
                    int currentPage = mPagination.getCurrentPage();
                    int pageCount = mPagination.getPageCount();
                    previousButton.setVisibility(View.VISIBLE);
                    nextButton.setVisibility(View.VISIBLE);

                    if (currentPage == 1 && pageCount == 1) {
                        previousButton.setVisibility(View.INVISIBLE);
                        nextButton.setVisibility(View.INVISIBLE);
                    }

                    if (currentPage == 1) {
                        previousButton.setVisibility(View.INVISIBLE);
                    }

                    if (currentPage == pageCount) {
                        nextButton.setVisibility(View.INVISIBLE);
                    }

                    tablePaginationDetails.setText(getString(R.string.table_pagination_details, String
                            .valueOf(currentPage), String.valueOf(itemsStart), String.valueOf(itemsEnd)));

                }
            };
    @NonNull
    private AdapterView.OnItemSelectedListener mItemSelectionListener = new AdapterView
            .OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            // 0. index is for empty item of spinner.
            if (position > 0) {

                String filter = Integer.toString(position);

                if (parent == moodFilter) {
                    filterTableForMood(filter);
                } else if (parent == genderFilter) {
                    filterTableForGender(filter);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Left empty intentionally.
        }
    };
    @NonNull
    private TextWatcher mSearchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            filterTable(String.valueOf(s));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    @NonNull
    private AdapterView.OnItemSelectedListener onItemsPerPageSelectedListener = new AdapterView
            .OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            int itemsPerPage;
            if ("All".equals(parent.getItemAtPosition(position).toString())) {
                itemsPerPage = 0;
            } else {
                itemsPerPage = Integer.parseInt(parent.getItemAtPosition(position).toString());
            }

            setTableItemsPerPage(itemsPerPage);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
    @NonNull
    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == previousButton) {
                previousTablePage();
            } else if (v == nextButton) {
                nextTablePage();
            }
        }
    };
    @NonNull
    private TextWatcher onPageTextChanged = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int page;
            if (TextUtils.isEmpty(s)) {
                page = 1;
            } else {
                page = Integer.parseInt(String.valueOf(s));
            }

            goToTablePage(page);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    //widget in attendance dialog
    private ImageView btnAddImage;
    private ImageView displayUploadedImage;
    private Button btnAttendDialog;
    //widget in add student dialog
    private EditText nameStudent, email, studentID, phoneStudent;
    private Button btnAddStuDialog, btnAddStuDialogCancel;
    private Spinner spinnerClass;
    private Button btnImportFile;
    private ImageView imvGetFile;
    //widget in spinner fetch student
    private Button btnFetch;
    private boolean checkInputImage = false;

    public StudentFragment() {

    }

    public void selectImage() {
        final CharSequence[] items = {"Máy Ảnh", "Thư Viện", "Hủy"};
        AlertDialog.Builder builder = new MaterialAlertDialogBuilder(Objects.requireNonNull(getActivity()));
        builder.setTitle("Thêm Ảnh");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("Máy Ảnh")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[which].equals("Thư Viện")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Chọn Ảnh"), SELECT_FILE);
                } else if (items[which].equals("Hủy")) {
                    dialog.dismiss();
                }
            }
        }).show();
    }

    private void setUpView(View layout) {
//            mDialogAttendance = layout.findV
        EditText searchField = layout.findViewById(R.id.query_string);
        searchField.addTextChangedListener(mSearchTextWatcher);
        Spinner itemsPerPage = layout.findViewById(R.id.items_per_page_spinner);
        View tableTestContainer = layout.findViewById(R.id.table_test_container);
        btnAttend = layout.findViewById(R.id.btnAttendance);
        tvDate = layout.findViewById(R.id.tvDate);
        btnPickDate = layout.findViewById(R.id.pickDate);
        btnPickDate.setOnClickListener(this);
        btnAttend.setOnClickListener(this);
        previousButton = layout.findViewById(R.id.previous_button);
        nextButton = layout.findViewById(R.id.next_button);
        EditText pageNumberField = layout.findViewById(R.id.page_number_text);
        tablePaginationDetails = layout.findViewById(R.id.table_details);
        studentViewModel = new ViewModelProvider(requireActivity()).get(StudentViewModel.class);
        classViewModel = new ViewModelProvider(requireActivity()).get(ClassViewModel.class);
        if (mPaginationEnabled) {
            tableTestContainer.setVisibility(View.VISIBLE);
            itemsPerPage.setOnItemSelectedListener(onItemsPerPageSelectedListener);

            previousButton.setOnClickListener(mClickListener);
            nextButton.setOnClickListener(mClickListener);
            pageNumberField.addTextChangedListener(onPageTextChanged);
        } else {
            tableTestContainer.setVisibility(View.GONE);
        }

        // Let's get TableView
        mTableView = layout.findViewById(R.id.tableview);
        mTableView.setHasFixedWidth(false);
        mTableView.setRowHeaderWidth(300);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_student, container, false);

        Bundle bundle = new Bundle();
        if (getArguments() != null) {
            bundle = getArguments();
            keyClass = bundle.getString("KeyClass");
            Log.d("AAAAA", keyClass);
        }
        //action bar custom
        actionBar();
        //
        initFabActionMenu(layout);
        setUpView(layout);
        new Thread(new Runnable() {
            @Override
            public void run() {

            }
        }).start();
        initializeTableView();

        if (mPaginationEnabled) {
            mTableFilter = new Filter(mTableView); // Create an instance of a Filter and pass the
            // created TableView.

            // Create an instance for the TableView pagination and pass the created TableView.
            mPagination = new Pagination(mTableView);

            // Sets the pagination listener of the TableView pagination to handle
            // pagination actions. See onTableViewPageTurnedListener variable declaration below.
            mPagination.setOnTableViewPageTurnedListener(onTableViewPageTurnedListener);
        }
        //


        return layout;
    }

    private void actionBar() {
//        BottomNavigationView navView = getActivity().findViewById(R.id.nav_view);
//        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        Toolbar toolbar = getActivity().findViewById(R.id.topAppBar);
        toolbar.setVisibility(View.VISIBLE);
        Fragment navHostFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        navHostFragment.getChildFragmentManager().getFragments().get(0);
        if(navHostFragment.getChildFragmentManager().getFragments().get(0) instanceof StudentFragment){
            toolbar.setTitle("Quản lý học sinh");
            Menu menu = toolbar.getMenu();
            //Check search icon isVisible
            if(!menu.findItem(R.id.top_app_bar_menu_search).isVisible()){
                menu.findItem(R.id.top_app_bar_menu_search).setVisible(true);
            }
            if(menu.findItem(R.id.top_app_bar_menu_add).isVisible()){
                menu.findItem(R.id.top_app_bar_menu_add).setVisible(false);
            }
            if(menu.findItem(R.id.top_app_bar_menu_bookmark).isVisible()){
                menu.findItem(R.id.top_app_bar_menu_bookmark).setVisible(false);
            }
        }
    }

    private void requestPm() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // start runtime permission
            Boolean hasPermission = (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED);
            if (!hasPermission) {
                Log.e(TAG, "get permision   ");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_EXTERNAL_STORAGE);
            } else {
                Log.e(TAG, "get permision-- already granted ");
                showFileChooser();
            }
        } else {
            //readfile();
            showFileChooser();
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        // Set your required file type
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Open File Chooser"), REQUEST_OPEN_FILE_CHOOSER);
    }

    private void initFabActionMenu(View layout) {
        fabWithOptions = layout.findViewById(R.id.fab);

        //Set mini fab's colors.
        fabWithOptions.setMiniFabsColors(
                R.color.colorPrimary,
                R.color.green_fab,
                R.color.secondaryColor,
                R.color.colorAccent);

        //Set main fab clicklistener.
        fabWithOptions.setMainFabOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Main fab clicked!", Toast.LENGTH_SHORT).show();
            }
        });

        //Set mini fabs clicklisteners.

        fabWithOptions.setMiniFabSelectedListener(new OptionsFabLayout.OnMiniFabSelectedListener() {
            @Override
            public void onMiniFabSelected(MenuItem fabItem) {
                switch (fabItem.getItemId()) {
                    case R.id.fab_addStudent:
                        mDialogAddStudent = new Dialog(getContext());
                        mDialogAddStudent.setContentView(R.layout.dialog_add_student);
                        mDialogAddStudent.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                        mDialogAddStudent.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        nameStudent = mDialogAddStudent.findViewById(R.id.edtnameStudentDialog);
                        email = mDialogAddStudent.findViewById(R.id.edtEmailStuDialog);
                        studentID = mDialogAddStudent.findViewById(R.id.edtMsvStuDialog);
                        phoneStudent = mDialogAddStudent.findViewById(R.id.edtPhoneStuDialog);
                        btnAddStuDialog = mDialogAddStudent.findViewById(R.id.btn_dialog_addStu_add);
                        btnAddStuDialogCancel = mDialogAddStudent.findViewById(R.id.btn_dialog_addStu_cancels);
                        studentDAO = StudentDAO.getInstance();
                        mDialogAddStudent.show();
                        btnAddStuDialogCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.d(TAG, "IN Cancel");
                                mDialogAddStudent.cancel();
                                fabWithOptions.closeOptionsMenu();
                            }
                        });
                        btnAddStuDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String name, mail, ID, phone;
                                name = nameStudent.getText().toString();
                                mail = email.getText().toString();
                                ID = studentID.getText().toString();
                                phone = phoneStudent.getText().toString();
                                Student stu = new Student(mail, keyClass, name, ID, phone);
                                if (name.equalsIgnoreCase("")) {//neu ton tai
                                    Toast.makeText(getActivity(), "Không được để trống tên", Toast.LENGTH_SHORT).show();

                                }
                                if (ID.equalsIgnoreCase("")) {//neu ton tai
                                    Toast.makeText(getActivity(), "Không được để trống mã sinh viên", Toast.LENGTH_SHORT).show();

                                }
                                boolean checkExsited = false;
                                for (Student s : listStu) {
                                    if (s.getMsv().equalsIgnoreCase(stu.getMsv())) {
                                        //da ton tai trong lop nay roi
                                        checkExsited = true;
                                        break;
                                    }
                                }
                                if (checkExsited) {//neu ton tai
                                    Toast.makeText(getActivity(), "Học sinh đã có trong lớp", Toast.LENGTH_SHORT).show();

                                } else {
                                    int numberOfStu = listStu.size();
                                    studentViewModel.addStudent(stu, days, numberOfStu);
                                    mDialogAddStudent.cancel();
                                    fabWithOptions.closeOptionsMenu();

                                }

                            }
                        });
                        btnAddStuDialogCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialogAddStudent.cancel();
                            }
                        });
                        break;
                    case R.id.fab_attendance:
//                        Toast.makeText(getActivity(),
//                                fabItem.getTitle() + "clicked!",
//                                Toast.LENGTH_SHORT).show();
                        mDialogAttendance = new Dialog(getContext());
                        mDialogAttendance.setContentView(R.layout.dialog_attendance);
                        mDialogAttendance.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                        mDialogAttendance.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        checkInputImage = false;
                        btnAddImage = mDialogAttendance.findViewById(R.id.uploadPhotoDiaglog);
                        displayUploadedImage = mDialogAttendance.findViewById(R.id.uploadedPhotoDiaglog);
                        btnAttendDialog = mDialogAttendance.findViewById(R.id.btnAttendDialog);
                        btnAddImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectImage();
                                checkInputImage = true;
                            }
                        });
                        //send photo to Service
                        btnAttendDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AttendeeRepository.getInstance().attendance(realPath, new AttendeeRepository.onAttendanceListener() {
                                    @Override
                                    public void onAttendance(List<String> listAttendee) {
                                        if (checkInputImage == false) {
                                            Toast.makeText(getContext(), "Bạn phải chọn ảnh để điểm danh trước", Toast.LENGTH_SHORT).show();
                                        }
                                        if (listAttendee != null && listAttendee.size() > 0) {
//                                            Toast.makeText(getContext(),listAttendee.get(0),Toast.LENGTH_SHORT).show();
                                            //them 1 day o day
                                            Date c = Calendar.getInstance().getTime();
                                            System.out.println("Current time => " + c);
                                            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                                            String today = df.format(c);
                                            Log.d(TAG, "TODAY: " + today);
                                            List<Attendee> attendees = new ArrayList<>();
                                            for (Student student : listStu) {
                                                attendees.add(new Attendee(false, student.getMsv()));
                                                for (String msvAttend : listAttendee) {
//                                                    "[" + student.getMsv() + "]"
                                                    if (msvAttend.equalsIgnoreCase(student.getMsv())) {
                                                        attendees.set(attendees.size() - 1, new Attendee(true, student.getMsv()));
//                                                        Toast.makeText(getContext(),student.getMsv(),Toast.LENGTH_SHORT).show();
                                                        break;
                                                    }
                                                }
                                            }
                                            AttendeeDAO.getInstance().attend(attendees, today, keyClass);
                                            mDialogAttendance.cancel();
                                        }
                                    }
                                });

                            }
                        });

                        mDialogAttendance.show();
                        mDialogAttendance.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                fabWithOptions.closeOptionsMenu();
                            }
                        });

                        break;
                    case R.id.fab_fetchData:
//                        Toast.makeText(getActivity(), "ABC", Toast.LENGTH_SHORT).show();
                        mDialogFetchTable = new Dialog(getContext());
                        mDialogFetchTable.setContentView(R.layout.dialog_fetch_student);
                        mDialogFetchTable.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                        mDialogFetchTable.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        btnFetch = mDialogFetchTable.findViewById(R.id.btnFetchStudents);
                        spinnerClass = mDialogFetchTable.findViewById(R.id.spinner_name_class);

                        List<String> namesClass = new ArrayList<>();
                        for (Class itemClass : spinnerClasses) {
                            namesClass.add(itemClass.getNameOfClass());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Objects.requireNonNull(getActivity()).getApplication(), android.R.layout.simple_spinner_item, namesClass);
                        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                        spinnerClass.setAdapter(adapter);
                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String keyClasses = spinnerClasses.get(position).getKeyClass();
                                System.out.println(keyClasses);
                                Log.d(TAG, "KeyClassSelected: " + keyClasses);
                                btnFetch.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        StudentDAO.getInstance().getAllStudent(keyClasses, new StudentDAO.OnStudentListener() {
                                            @Override
                                            public void onReceiveData(List<Student> list) {
                                                List<Student> listStudent = new ArrayList<>();
                                                for (Student stu : list) {
                                                    listStudent.add(new Student(stu.getEmail(), keyClass, stu.getFullName(), stu.getMsv(), stu.getPhoneNumber()));
                                                }
                                                Log.d(TAG, "Size Student: " + listStudent.size());
                                                StudentDAO.getInstance().addStudents(listStudent);
                                                studentViewModel.setStudents(listStudent);
                                            }
                                        });
                                        mDialogFetchTable.cancel();
                                    }
                                });
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
//                        classViewModel.getClasses().observe(getViewLifecycleOwner(), new Observer<List<Class>>() {
//                            @Override
//                            public void onChanged(List<Class> classList) {
//
//                            }
//                        });
                        mDialogFetchTable.show();
                        mDialogFetchTable.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                fabWithOptions.closeOptionsMenu();
                            }
                        });
                        break;
                    case R.id.fab_importFileStudent:
                        requestPm();
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSpinner();
    }

    @Override
    public void onResume() {
        super.onResume();
        initSpinner();
    }

    private void initSpinner() {
        FirebaseDAO.getInstance().getKeyTeacher(new FirebaseDAO.OnKeyTeacherListener() {
            @Override
            public void onReceiveKey(String keyTeacher) {
                FirebaseDAO.getInstance().getAllClassesRealTime(keyTeacher, new DataLoadListener() {
                    @Override
                    public void onListLoaded(List<Class> listClass) {
                        if (spinnerClasses.size() > 0) {
                            spinnerClasses.clear();
                            spinnerClasses = listClass;
                        } else {
                            spinnerClasses = listClass;
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.fab_menu, menu);
    }

    private void initializeTableView() {
        // Create TableView View model class  to group view models of TableView
        tableViewModel = new TableViewModel();

        // Create TableView Adapter
        TableViewAdapter tableViewAdapter = new TableViewAdapter(tableViewModel);
        List<Cell> cellList = new ArrayList<>();
        for (int j = 0; j < 200; j++) {
            Object text = "Tran Chi Quang ";
            // Create dummy id.
            String id = j + "-";
            Cell cell;
            cell = new Cell(id, text);
            cellList.add(cell);
        }
        attendeeDAO = AttendeeDAO.getInstance();
        mTableView.setAdapter(tableViewAdapter);
//        tableViewAdapter.addRow(0, new RowHeader("2", "TEST"), cellList);
        //hide floating button
        hidingFloatingButtononScrolled(mTableView);

        // Create an instance of a Filter and pass the TableView.z
        //mTableFilter = new Filter(mTableView);

        // Load the dummy data to the TableView

        StudentDAO studentDAO = StudentDAO.getInstance();
        studentDAO.getAllStudent(keyClass, new StudentDAO.OnStudentListener() {
            @Override
            public void onReceiveData(List<Student> list) {
                Log.d(TAG, keyClass);
//                studentViewModel.setStudents(list);

                studentViewModel.setStudents(list);
                studentViewModel.getStudents().observe(getViewLifecycleOwner(), new Observer<List<Student>>() {
                    @Override
                    public void onChanged(List<Student> students) {
                        attendeeDAO.getAttendeeByClass(keyClass, new AttendeeDAO.OnAttendeesListener() {
                            @Override
                            public void onReceiveAttendees(Map<String, List<Attendee>> maps) {
                                List<String> listDays = new ArrayList<>();
                                Set<String> set = maps.keySet();
                                for (String key : set) {
                                    listDays.add(key);
                                    System.out.println(maps.get(key).get(0).getKeyStudent());
                                }
                                days = listDays;
                                //Start init Tieu de cot
                                List<ColumnHeader> listColumnHeader = tableViewModel.getColumnHeaderList();
                                int lenColumnHeader = listDays.size();
                                for (int i = 0; i < lenColumnHeader; i++) {
                                    listColumnHeader.set(i, new ColumnHeader("DAY", listDays.get(i)));
                                }
                                //end
                                //Start init Tieu de hang
                                List<RowHeader> listRowHeader = tableViewModel.getRowHeaderList();
                                int len = students.size();
                                Log.d(TAG, "SIZE STUDENT: " + len);
                                listStu = students;
                                for (int i = 0; i < len; i++) {
                                    Student student = students.get(i);
                                    listRowHeader.set(i, new RowHeader(student.getMsv(), student.getFullName()));
                                }
                                //end
//                                maps.get(listDays.get(i)).get(j).isAttended())
                                //start init cell
                                List<List<Cell>> cells = tableViewModel.getCellList();
//                                Log.d(TAG,"Recently Key Student: "+listStu.get(len-1).getFullName());
                                for (int i = 0; i < len; i++) {//list theo hàng là danh sách học sinh
                                    List<Cell> cellList = cells.get(i);//lấy từng sinh viên để duyệt các ngày sinh viên đó
                                    String msv = listStu.get(i).getMsv();

                                    for (int j = 0; j < lenColumnHeader; j++) {//list theo cột là danh sách ngày
                                        String days = listDays.get(j);
                                        //duyet di hay k di cho key Stu phia tren cua moi ngay
                                        List<Attendee> listOfDay = maps.get(days);
                                        boolean isAttended = getAttended(msv, listOfDay);
//                                        boolean isAttended = true;
//                                        Object text = Boolean.toString(maps.get(listDays.get(j)).get(i).isAttended());
                                        String att = "";
                                        if (isAttended == true) {
                                            att = "V";
                                        } else {
                                            att = "X";
                                        }
                                        Object text = att;
//                                       Object text = "Tran Chi Quang " + j + " " + i;
                                        // Create dummy id.
                                        String id = j + "-" + i;
                                        Cell cell;
                                        cell = new Cell(id, text);
                                        cellList.set(j, cell);
                                    }
                                    cells.set(i, cellList);
                                }
                                //end

                                //fetch Data to table
                                tableViewAdapter.setAllItems(listColumnHeader,
                                        listRowHeader, cells);

                            }
                        });

                    }
                });


            }
        });


        //mTableView.setHasFixedWidth(true);

        /*for (int i = 0; i < mTableViewModel.getCellList().size(); i++) {
            mTableView.setColumnWidth(i, 200);
        }*)

        //mTableView.setColumnWidth(0, -2);
        //mTableView.setColumnWidth(1, -2);

        /*mTableView.setColumnWidth(2, 200);
        mTableView.setColumnWidth(3, 300);
        mTableView.setColumnWidth(4, 400);
        mTableView.setColumnWidth(5, 500);*/

    }

    private boolean getAttended(String keyStu, List<Attendee> listOfDay) {
        boolean isAttended = false;
        Log.d(TAG, "ListStuDay" + listOfDay.size());
        for (Attendee attendee : listOfDay) {
            if (keyStu.equalsIgnoreCase(attendee.getMsv())) {
                isAttended = attendee.isAttended();
                break;
            }
        }
        return isAttended;
    }

    private void hidingFloatingButtononScrolled(TableView mTableView) {
        mTableView.setTableViewListener(new TableViewListener(mTableView));
        mTableView.getRowHeaderRecyclerView().addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) fabWithOptions.setVisibility(View.INVISIBLE);
                else fabWithOptions.setVisibility(View.VISIBLE);
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    public void filterTable(@NonNull String filter) {
        // Sets a filter to the table, this will filter ALL the columns.
        mTableFilter.set(filter);
    }

    public void filterTableForMood(@NonNull String filter) {
        // Sets a filter to the table, this will only filter a specific column.
        // In the example data, this will filter the mood column.
        mTableFilter.set(TableViewModel.MOOD_COLUMN_INDEX, filter);
    }

    public void filterTableForGender(@NonNull String filter) {
        // Sets a filter to the table, this will only filter a specific column.
        // In the example data, this will filter the gender column.
        mTableFilter.set(TableViewModel.GENDER_COLUMN_INDEX, filter);
    }

    // The following four methods below: nextTablePage(), previousTablePage(),
    // goToTablePage(int page) and setTableItemsPerPage(int itemsPerPage)
    // are for controlling the TableView pagination.
    public void nextTablePage() {
        mPagination.nextPage();
    }

    public void previousTablePage() {
        mPagination.previousPage();
    }

    public void goToTablePage(int page) {
        mPagination.goToPage(page);
    }

    public void setTableItemsPerPage(int itemsPerPage) {
        mPagination.setItemsPerPage(itemsPerPage);
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d("LoadImage", "IN");
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == REQUEST_CAMERA) {
                Uri uri = data.getData();
                realPath = getRealPathFromURI(uri);
                Bundle bundle = data.getExtras();
                final Bitmap bitmap = (Bitmap) bundle.get("data");
                displayUploadedImage.setImageBitmap(bitmap);
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                realPath = getRealPathFromURI(selectedImageUri);
                displayUploadedImage.setImageURI(selectedImageUri);
            } else if (requestCode == REQUEST_OPEN_FILE_CHOOSER && data != null) {
                if (resultCode == RESULT_OK) {
                    boolean checkChooseFile = true;
                    Uri currFileURI = data.getData();
                    String path = currFileURI.getPath();
                    Log.d("PathPickfile", "sss" + currFileURI.getAuthority());
                    String fullerror = "";
                    try {
                        Uri imageuri = data.getData();
                        InputStream stream = null;
                        String tempID = "", id = "";

                        Uri uri = data.getData();
                        Log.e(TAG, "file auth is " + uri.getAuthority());
                        fullerror = fullerror + "file auth is " + uri.getAuthority();

                        if (imageuri.getAuthority().equals("media")) {
                            tempID = imageuri.toString();
                            tempID = tempID.substring(tempID.lastIndexOf("/") + 1);
                            id = tempID;
                            Uri contenturi = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                            String selector = MediaStore.Images.Media._ID + "=?";
                            actualfilepath = getColunmData(contenturi, selector, new String[]{id});


                        } else if (imageuri.getAuthority().equals("com.android.providers.media.documents")) {
                            tempID = DocumentsContract.getDocumentId(imageuri);
                            String[] split = tempID.split(":");
                            String type = split[0];
                            id = split[1];
                            Uri contenturi = null;
                            if (type.equals("image")) {
                                contenturi = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                            } else if (type.equals("video")) {
                                contenturi = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                            } else if (type.equals("audio")) {
                                contenturi = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                            }
                            String selector = "_id=?";
                            actualfilepath = getColunmData(contenturi, selector, new String[]{id});

                        } else if (imageuri.getAuthority().equals("com.android.providers.downloads.documents")) {

                            tempID = imageuri.toString();
                            tempID = tempID.substring(tempID.lastIndexOf("/") + 1);
                            id = tempID;
                            Uri contenturi = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                            // String selector = MediaStore.Images.Media._ID+"=?";
                            actualfilepath = getColunmData(contenturi, null, null);


                        } else if (imageuri.getAuthority().equals("com.android.externalstorage.documents")) {

                            tempID = DocumentsContract.getDocumentId(imageuri);
                            String[] split = tempID.split(":");
                            String type = split[0];
                            id = split[1];
                            Uri contenturi = null;
                            if (type.equals("primary")) {
                                actualfilepath = Environment.getExternalStorageDirectory() + "/" + id;
                            }
                        } else {
                            fabWithOptions.closeOptionsMenu();
                            checkChooseFile = false;
                            Toast.makeText(getContext(), "Chọn File không hợp lệ", Toast.LENGTH_SHORT).show();
                        }
                        if (checkChooseFile == true) {
                            File myFile = new File(actualfilepath);
                            // MessageDialog dialog = new MessageDialog(Home.this, " file details --"+actualfilepath+"\n---"+ uri.getPath() );
                            // dialog.displayMessageShow();
                            String temppath = uri.getPath();
                            if (temppath.contains("//")) {
                                temppath = temppath.substring(temppath.indexOf("//") + 1);
                            }
                            Log.e(TAG, " temppath is " + temppath);
                            fullerror = fullerror + "\n" + " file details -  " + actualfilepath + "\n --" + uri.getPath() + "\n--" + temppath;

                            if (actualfilepath.equals("") || actualfilepath.equals(" ")) {
                                myFile = new File(temppath);

                            } else {
                                myFile = new File(actualfilepath);
                            }

                            //File file = new File(actualfilepath);
                            //Log.e(TAG, " actual file path is "+ actualfilepath + "  name ---"+ file.getName());

//                    File myFile = new File(actualfilepath);
                            Log.e(TAG, " myfile is " + myFile.getAbsolutePath());
//                        File finalMyFile = myFile;
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                try {
//                                    readfile(finalMyFile);
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }).start();
                            k = new readFileStudentTask();
                            k.execute(myFile);

                            // lyf path  - /storage/emulated/0/kolektap/04-06-2018_Admin_1528088466207_file.xls
                        }


                    } catch (Exception e) {

                    }
                }


            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }

    public String getColunmData(Uri uri, String selection, String[] selectarg) {

        String filepath = "";
        Cursor cursor = null;
        String colunm = "_data";
        String[] projection = {colunm};
        cursor = getActivity().getContentResolver().query(uri, projection, selection, selectarg, null);
        if (cursor != null) {
            cursor.moveToFirst();

            Log.e(TAG, " file path is " + cursor.getString(cursor.getColumnIndex(colunm)));
            filepath = cursor.getString(cursor.getColumnIndex(colunm));

        }
        if (cursor != null)
            cursor.close();

        return filepath;
    }

    public List<Student> readfile(File file) throws IOException {


        Log.e(TAG, "IN");
        InputStream myInput;
        myInput = new FileInputStream(file);
        XSSFWorkbook workbook = new XSSFWorkbook(myInput);
        XSSFSheet sheet = workbook.getSheetAt(0);
        int rowsCount = sheet.getPhysicalNumberOfRows();
        FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
        StringBuilder sb = new StringBuilder();
        Log.e(TAG, "Num: " + rowsCount);
        List<Student> fileListStudent = new ArrayList<>();
        //outter loop, loops through rows
        for (int r = 1; r < rowsCount; r++) {
            Row row = sheet.getRow(r);
            int cellsCount = row.getPhysicalNumberOfCells();
            Log.e(TAG, "Num Cell: " + cellsCount);
            //inner loop, loops through columns
            for (int c = 0; c < cellsCount; c++) {
                //handles if there are to many columns on the excel sheet.
                String value = new CellExcel().getCellAsString(row, c, formulaEvaluator);
                String cellInfo = "r:" + r + "; c:" + c + "; v:" + value;
                Log.d(TAG, "readExcelData: Data from row: " + cellInfo);
                sb.append(value + ", ");

            }
            String msv = new CellExcel().getCellAsString(row, 1, formulaEvaluator);
            String fullname = new CellExcel().getCellAsString(row, 2, formulaEvaluator);
            String email = new CellExcel().getCellAsString(row, 3, formulaEvaluator);
            String phoneNumber = new CellExcel().getCellAsString(row, 4, formulaEvaluator);
            Student stu = new Student(email, keyClass, fullname, msv, phoneNumber);
            fileListStudent.add(stu);
            sb.append(":");


        }

//        studentViewModel.setStudents(fileListStudent);
//        progressDialog.dismiss();
        Log.d(TAG, "readExcelData: STRINGBUILDER: " + sb.toString());
        return fileListStudent;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //readfile();
                    showFileChooser();
                } else {
                    // show a msg to user
                }
                break;
        }
    }

    private class readFileStudentTask extends AsyncTask<File, Void, List<Student>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.show();
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }

        @Override
        protected List<Student> doInBackground(File... files) {
            try {
                List<Student> listStu = readfile(files[0]);
                return listStu;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(List<Student> students) {
            super.onPostExecute(students);
            progressDialog.dismiss();
            studentViewModel.setStudents(students);
            StudentDAO.getInstance().addStudents(students);
            fabWithOptions.closeOptionsMenu();
        }
    }
}

