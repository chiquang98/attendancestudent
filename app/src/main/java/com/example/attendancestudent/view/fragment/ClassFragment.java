package com.example.attendancestudent.view.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendancestudent.R;
import com.example.attendancestudent.adapter.ClassRecycleAdapter;
import com.example.attendancestudent.listenerinterface.DataLoadListener;
import com.example.attendancestudent.listenerinterface.OnItemClickListener;
import com.example.attendancestudent.model.Class;
import com.example.attendancestudent.repository.firebase.FirebaseDAO;
import com.example.attendancestudent.viewmodel.ClassViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClassFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = ClassFragment.class.getSimpleName();
    ClassRecycleAdapter classRecycleAdapter;
    FirebaseDAO firebaseDAO;
    Dialog mDialog, mDialogAddClass;
    List<Class> listclasses = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private FloatingActionButton mActionButton;
    private boolean onStudentFragment = false;
    private NavController navController;
    private ClassViewModel classViewModel;
    private String IDTEACHER = null;

    public ClassFragment() {
        // Required empty public constructor

    }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (onStudentFragment == true) {
            outState.putBoolean("OnStudentFrag", true);
        } else {
            outState.putBoolean("OnStudentFrag", false);
        }

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //     layout for this fragment

        View view = inflater.inflate(R.layout.fragment_class, container, false);
        Log.d(TAG,"onCreateView");

        actionBar();
        setUpView(view);
        setUpRecyclerView();
        showHideWhenScroll();

//        realTimeUpdateClass();
        return view;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
        firebaseDAO = FirebaseDAO.getInstance();

//        initListClass();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab://button add a class - floatbutton
                System.out.println("Innnnnnn");
                mDialogAddClass = new Dialog(getContext());
                mDialogAddClass.setContentView(R.layout.dialog_add_class);
                EditText nameClass = mDialogAddClass.findViewById(R.id.editTextAddClass);
                EditText subject = mDialogAddClass.findViewById(R.id.editTextSubject);
                Button btnAdd = mDialogAddClass.findViewById(R.id.btn_dialog_addClass_Add);
                //button add in Dialog Add Class
                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Class tempClass = new Class();
                        tempClass.setIdTea(IDTEACHER);
                        tempClass.setNameOfClass(nameClass.getText().toString());
                        tempClass.setSubject(subject.getText().toString());
                        System.out.println(listclasses.size());
                        listclasses.add(tempClass);
                        System.out.println(listclasses.size());
                        classRecycleAdapter.notifyItemInserted(listclasses.size()-1);
                        FirebaseDAO.getInstance().addClass(tempClass);
//                        classViewModel.addClass(tempClass);
                        mDialogAddClass.cancel();
                    }
                });
                mDialogAddClass.show();
                break;
        }
    }
    private void actionBar() {
        //check fragment
        Toolbar toolbar = getActivity().findViewById(R.id.topAppBar);
        toolbar.setVisibility(View.VISIBLE);
        Fragment navHostFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        navHostFragment.getChildFragmentManager().getFragments().get(0);
        if(navHostFragment.getChildFragmentManager().getFragments().get(0) instanceof ClassFragment){
            toolbar.setTitle("Danh sách lớp");
            Menu menu = toolbar.getMenu();
            //Check search icon isVisible
            if(!menu.findItem(R.id.top_app_bar_menu_search).isVisible()){
                menu.findItem(R.id.top_app_bar_menu_search).setVisible(true);
            }
            if(menu.findItem(R.id.top_app_bar_menu_add).isVisible()){
                menu.findItem(R.id.top_app_bar_menu_add).setVisible(false);
            }
            if(menu.findItem(R.id.top_app_bar_menu_bookmark).isVisible()){
                menu.findItem(R.id.top_app_bar_menu_bookmark).setVisible(false);
            }
        }
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.top_app_bar_menu_search:
                        Log.e("OptionMenu","S");
                        SearchView searchView = (SearchView) item.getActionView();
                        searchView.setQueryHint("Nhập để tìm kiếm lớp");
                        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
                        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                            @Override
                            public boolean onQueryTextSubmit(String query) {
                                return false;
                            }

                            @Override
                            public boolean onQueryTextChange(String newText) {
                                Log.e("OptionMenu","S");
                                classRecycleAdapter.getFilter().filter(newText);
                                return false;
                            }
                        });
                        break;
                    default:

                        break;
                }

                return false;
            }
        });
    }
    private void setUpView(View view) {
        BottomNavigationView navView = getActivity().findViewById(R.id.nav_view);
//        classViewModel = new ViewModelProvider(requireActivity()).get(ClassViewModel.class);
        navView.setVisibility(View.VISIBLE);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        //button add class - float button
        mActionButton = view.findViewById(R.id.fab);
        mActionButton.setEnabled(false);
        mActionButton.setOnClickListener(this);
    }

    private void setUpRecyclerView() {
        onStudentFragment = false;
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager
                .VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        firebaseDAO = FirebaseDAO.getInstance();
        initListClass();
        classRecycleAdapter = new ClassRecycleAdapter(getContext(), listclasses);
        mRecyclerView.setAdapter(classRecycleAdapter);
//        initListClass();
        onClickItem();
//        clickOptionClassItem();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    private void initListClass() {
        FirebaseDAO.getInstance().getKeyTeacher(new FirebaseDAO.OnKeyTeacherListener() {
            @Override
            public void onReceiveKey(String keyTeacher) {
                String keyTea = keyTeacher;
                IDTEACHER = keyTeacher;
                FirebaseDAO.getInstance().getAllClasses(keyTea, new DataLoadListener() {
                    @Override
                    public void onListLoaded(List<Class> listClass) {
                        Log.d(TAG,"On Key teacher Changed");
//                        classViewModel.setListClass(listClass);
                        classRecycleAdapter.setClasses(listClass);
                        listclasses = listClass;
                        mActionButton.setEnabled(true);
                    }
                });
            }
        });
    }

    private void showHideWhenScroll() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //dy > 0: scroll up; dy < 0: scroll down
                if (dy > 0) mActionButton.hide();
                else mActionButton.show();
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void onClickItem() {
        classRecycleAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClicked(int position, String keyClass) {
                Log.d(TAG, keyClass);
                Bundle bundle = new Bundle();
                bundle.putString("KeyClass", keyClass);
//                classViewModel.setKeyTeacher(keyClass);
                navController.navigate(R.id.action_bottom_nav_class_to_studentFragment, bundle);
            }
        });
    }



    private void clickOptionClassItem() {
        classRecycleAdapter.setOnClickMenuClassOptionListener(new ClassRecycleAdapter.OnClickMenuClassOptionListener() {
            @Override
            public void onClicked(int positionRow, int positionOption, String keyClass) {
                switch (positionOption) {
                    case 0://delete
                        String key = keyClass;
//                        classViewModel.deleteClass(positionRow, key);
                        Log.d(TAG,"POSITION: "+positionRow);
                        listclasses.remove(positionRow);
//                        classRecycleAdapter.notifyItemRemoved(positionRow);
                        firebaseDAO.deleteClass(key);
                        break;
                    case 1://rename
                        mDialog = new Dialog(getContext());
                        mDialog.setContentView(R.layout.dialog_rename_class);
                        final EditText dialog_tvName = mDialog.findViewById(R.id.edt_dialog_rename_nameClass);
                        Button dialog_btnAdd = mDialog.findViewById(R.id.btn_dialog_rename_add);
                        Button dialog_btnCancel = mDialog.findViewById(R.id.btn_dialog_rename_cancel);
                        dialog_tvName.setText(listclasses.get(positionRow).getNameOfClass());
                        dialog_btnAdd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Class temp = listclasses.get(positionRow);
                                String rename_Name = dialog_tvName.getText().toString();
//                                classViewModel.renameClass(positionRow,rename_Name);
                                temp.setNameOfClass(rename_Name);
                                firebaseDAO.renameClass(keyClass,rename_Name);
                                listclasses.set(positionRow, temp);
                                classRecycleAdapter.notifyItemChanged(positionRow);
                                mDialog.cancel();
                            }
                        });
                        dialog_btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.cancel();
                            }
                        });
                        mDialog.show();
                        break;
                }
            }
        });
    }
}
