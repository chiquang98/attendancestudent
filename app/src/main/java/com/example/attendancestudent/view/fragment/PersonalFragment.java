package com.example.attendancestudent.view.fragment;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.example.attendancestudent.R;
import com.example.attendancestudent.listenerinterface.DataLoadListener;
import com.example.attendancestudent.listenerinterface.OnBackPressedListener;
import com.example.attendancestudent.model.Class;
import com.example.attendancestudent.model.Student;
import com.example.attendancestudent.model.Teacher;
import com.example.attendancestudent.repository.firebase.FirebaseAuthSingleton;
import com.example.attendancestudent.repository.firebase.FirebaseDAO;
import com.example.attendancestudent.repository.firebase.StudentDAO;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalFragment extends Fragment implements View.OnClickListener, OnBackPressedListener {
    private static final String TAG = PersonalFragment.class.getSimpleName();
    LinearLayout btnSendEmail,btnLogout;
    private Button btnSendEmailDialog;
    private TextView tvMGV,tvNameofTeacher;
    private FirebaseAuth mAuth;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authListener;
    private NavController navController;

    private Dialog mDialogFetchTable;

    public PersonalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        actionBar();
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal, container, false);
        initWidget(view);
        return view;
    }

    private void actionBar() {
        //check fragment
        Toolbar toolbar = getActivity().findViewById(R.id.topAppBar);
        Fragment navHostFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        navHostFragment.getChildFragmentManager().getFragments().get(0);
        if(navHostFragment.getChildFragmentManager().getFragments().get(0) instanceof PersonalFragment){
            toolbar.setTitle("Cá nhân");
            Menu menu = toolbar.getMenu();
            if(menu.findItem(R.id.top_app_bar_menu_search).isVisible()){
                menu.findItem(R.id.top_app_bar_menu_search).setVisible(false);
            }
            if(menu.findItem(R.id.top_app_bar_menu_add).isVisible()){
                menu.findItem(R.id.top_app_bar_menu_add).setVisible(false);
            }
            if(menu.findItem(R.id.top_app_bar_menu_bookmark).isVisible()){
                menu.findItem(R.id.top_app_bar_menu_bookmark).setVisible(false);
            }

        }
    }

    private void initWidget(View view) {
        btnLogout = view.findViewById(R.id.btnLogout);
        btnSendEmail = view.findViewById(R.id.btnSendEmail);
        btnSendEmail.setOnClickListener(this);
        tvMGV = view.findViewById(R.id.tvMGV);
        tvNameofTeacher = view.findViewById(R.id.tvNameOfTeacher);
        mAuth = FirebaseAuthSingleton.getInstance().getFirebaseAuth();
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                navController.navigate(R.id.action_bottom_nav_personal_to_loginFragment);
            }
        });

        auth = FirebaseAuth.getInstance();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null){
            //user is signed in
            String uid = user.getUid();
            loadInfoFromFirebase(uid);
        }
        else{
            navController.navigate(R.id.action_bottom_nav_personal_to_loginFragment);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSpinner();
        navController = Navigation.findNavController(view);
    }

    private void loadInfoFromFirebase(String uid) {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'users' node
        Log.d("UIDtester",uid);
        mFirebaseDatabase = mFirebaseInstance.getReference().child("Person");
        mFirebaseDatabase.child("Teachers").orderByChild("iduser").equalTo(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                System.out.println(mFirebaseDatabase.);
//                Teacher teacher = dataSnapshot.child("-M3AvED01eoI3uariLRs").getValue(Teacher.class);
                Teacher teacher = null;

                for(DataSnapshot child :dataSnapshot.getChildren()){
                    teacher = child.getValue(Teacher.class);
                    System.out.println(child.getKey());
                }
                Log.d("checkkk",dataSnapshot.exists()+"");
                Log.d("hihihi", dataSnapshot.toString());
                if(teacher!=null){
                    tvMGV.setText(teacher.getMgv());
                    tvNameofTeacher.setText(teacher.getFullName());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }
    private Spinner spinnerClass;
    private List<Class> classes = new ArrayList<>();
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSendEmail:
                mDialogFetchTable = new Dialog(getContext());
                mDialogFetchTable.setContentView(R.layout.dialog_send_email);
                mDialogFetchTable.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                mDialogFetchTable.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                btnSendEmailDialog = mDialogFetchTable.findViewById(R.id.btnSend);
                spinnerClass = mDialogFetchTable.findViewById(R.id.spinner_name_class_dialog_sendMail);
                List<String> namesClass = new ArrayList<>();
                for (Class itemClass : classes) {
                    namesClass.add(itemClass.getNameOfClass());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Objects.requireNonNull(getActivity()).getApplication(), android.R.layout.simple_spinner_item, namesClass);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                spinnerClass.setAdapter(adapter);
                spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String keyClasses = classes.get(position).getKeyClass();
                        System.out.println(keyClasses);
                        Log.d(TAG, "KeyClassSelected: " + keyClasses);
                        StringBuilder mails = new StringBuilder("mailto:");
                        btnSendEmailDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                StudentDAO.getInstance().getAllStudent(keyClasses, new StudentDAO.OnStudentListener() {
                                    @Override
                                    public void onReceiveData(List<Student> list) {
//                                        List<Student> listStudent = new ArrayList<>();

                                        for (Student stu : list) {
//                                            listStudent.add(new Student(stu.getEmail(), keyClasses, stu.getFullName(), stu.getMsv(), stu.getPhoneNumber()));
                                            mails.append(stu.getEmail());
                                            mails.append(",");
                                        }
                                        mails.deleteCharAt(mails.length()-1);
                                        Log.d(TAG,"Mail:"+mails.toString());
//                                        Log.d(TAG, "Size Student: " + listStudent.size());
                                        sendEmail(mails.toString());
                                    }
                                });
                                mDialogFetchTable.cancel();
                            }
                        });
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
                mDialogFetchTable.show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        initSpinner();
    }

    private void initSpinner() {
        FirebaseDAO.getInstance().getKeyTeacher(new FirebaseDAO.OnKeyTeacherListener() {
            @Override
            public void onReceiveKey(String keyTeacher) {
                FirebaseDAO.getInstance().getAllClassesRealTime(keyTeacher, new DataLoadListener() {
                    @Override
                    public void onListLoaded(List<Class> listClass) {
                        if (classes.size() > 0) {
                            classes.clear();
                            classes = listClass;
                        } else {
                            classes = listClass;
                        }
                    }
                });
            }
        });
    }
    private void sendEmail(String emails) {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SENDTO);

        emailIntent.setType("message/rfc822");
        emailIntent.setData(Uri.parse(emails));

        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "email content");

        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    @Override
    public void onBackPressed() {
        Fragment navHostFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
//        navHostFragment.getChildFragmentManager().getFragments().get(0);
        final Fragment currentFragment = navHostFragment.getChildFragmentManager().getFragments().get(0);
        final NavController controller = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        if (currentFragment instanceof OnBackPressedListener)
            ((OnBackPressedListener) currentFragment).onBackPressed();
        else if (!controller.popBackStack())
            getActivity().finish();
    }
}
