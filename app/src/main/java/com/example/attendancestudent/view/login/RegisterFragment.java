package com.example.attendancestudent.view.login;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.example.attendancestudent.R;

import com.example.attendancestudent.model.Teacher;
import com.example.attendancestudent.model.User;
import com.example.attendancestudent.repository.firebase.FirebaseAuthSingleton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment implements View.OnClickListener {
    private FirebaseAuth mAuth;
    private Button btnSignUp;
    private EditText edtEmail;
    private EditText edtPassword,edtMGV,edtFullName,edtPhoneNumber;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private NavController navController;
   private ImageView btnBack;
    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        System.out.println("inRegis");
        View view  = inflater.inflate(R.layout.fragment_register, container, false);
        mAuth = FirebaseAuthSingleton.getInstance().getFirebaseAuth();
        fragmentManager = getActivity().getSupportFragmentManager();
//        if(mAuth.getCurrentUser()!=null){
////            Intent i = new Intent(getActivity(), MainActivity.class);
//            startActivity(i);
//            ((Activity) getActivity()).overridePendingTransition(0, 0);
//        }
        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference().child("Users");
        btnSignUp = view.findViewById(R.id.cirRegisterButton);
        edtEmail = view.findViewById(R.id.editTextEmail);
        edtMGV = view.findViewById(R.id.editTextMSV);
        edtFullName = view.findViewById(R.id.editTextName);
        edtPhoneNumber = view.findViewById(R.id.editTextMobile);
        edtPassword = view.findViewById(R.id.editTextPassword);
        btnBack = view.findViewById(R.id.btnBackToLogin);
        btnBack.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cirRegisterButton:
                Log.d("check","in");
//                System.out.println(edtEmail.getText().toString());

                mAuth.createUserWithEmailAndPassword(edtEmail.getText().toString(),edtPassword.getText().toString()).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Toast.makeText(getContext(), "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                        System.out.println(edtEmail.getText().toString());
                        if(!task.isSuccessful()){//neu khong dang ki thanh cong
                            Toast.makeText(getContext(), "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{//neu dang ky thanh cong
                            String currentuser = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            Log.d("RegFribase",currentuser);
                            String email = edtEmail.getText().toString();
                            String password = edtPassword.getText().toString();
                            User user = new User(email,password);
                            mFirebaseDatabase.child(currentuser).setValue(user);
                            Teacher teacher = new Teacher(email,currentuser,edtFullName.getText().toString(),edtMGV.getText().toString(),edtPhoneNumber.getText().toString());
                            mFirebaseDatabase.getParent().child("Person").child("Teachers").push().setValue(teacher);
                            mAuth.signOut();//khong hieu sao khi tao thanh cong thi no signIn luon
                            fragmentTransaction = fragmentManager.beginTransaction();
                            Fragment loginFragment = new LoginFragment();
//                            fragmentTransaction.replace(R.id.fragment_login,loginFragment,null);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        }
                    }
                });
                break;
            case R.id.btnBackToLogin:
                navController.navigate(R.id.action_registerFragment_to_loginFragment);
                break;
        }
    }
}
