package com.example.attendancestudent.view.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.example.attendancestudent.R;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class AddEventFullScreenDialog extends DialogFragment implements View.OnClickListener {
    private static AddEventFullScreenDialog instance;
    ImageButton imgClose;
    TextView tvAddEvent,tvFromDate,tvEndDate,tvFromTime,tvEndTime,tvLocation,tvTitle;
    Calendar result = Calendar.getInstance();
    public static AddEventFullScreenDialog getInstance(){
        if(instance==null){
            return new AddEventFullScreenDialog();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_event,container,false);
        imgClose = view.findViewById(R.id.fullscreen_dialog_close);
        tvAddEvent = view.findViewById(R.id.fullscreen_dialog_action);
        tvFromDate = view.findViewById(R.id.tvFromDate);
        tvFromTime = view.findViewById(R.id.tvFromTime);
        tvEndDate = view.findViewById(R.id.tvEndDate);
        tvEndTime = view.findViewById(R.id.tvEndTime);
        tvTitle = view.findViewById(R.id.editTextTitle);
        tvLocation = view.findViewById(R.id.editText2);
        tvFromDate.setOnClickListener(this);
        tvFromTime.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
        tvEndTime.setOnClickListener(this);
        tvAddEvent.setOnClickListener(this);
        imgClose.setOnClickListener(this);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        tvFromDate.setText(simpleDateFormat.format(calendar.getTime()));
        tvEndDate.setText(simpleDateFormat.format(calendar.getTime()));
        simpleDateFormat = new SimpleDateFormat("HH:mm");
        tvFromTime.setText(simpleDateFormat.format(calendar.getTime()));
        tvEndTime.setText(simpleDateFormat.format(calendar.getTime()));

        return view;
    }
    Calendar calendarDateFrom = Calendar.getInstance();
    Calendar calendarDateEnd = Calendar.getInstance();
    private void setupDatePicker(TextView textView,Calendar calendarDate){

        // Lấy ra năm - tháng - ngày hiện tại
        int year = calendarDate.get(Calendar.YEAR);
        final int month = calendarDate.get(Calendar.MONTH);
        int day = calendarDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendarDate.set(year,month,dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                textView.setText(simpleDateFormat.format(calendarDate.getTime()));

            }
        },year,month,day);
        datePickerDialog.show();
    }
    Calendar calenderHourFrom = Calendar.getInstance();
    Calendar calenderHourEnd = Calendar.getInstance();
    private void setupTimePicker(TextView textView,Calendar calenderHour){

        int hour = calenderHour.get(Calendar.HOUR_OF_DAY);
        int min = calenderHour.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calenderHour.set(0,0,0,hourOfDay,minute);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                textView.setText(simpleDateFormat.format(calenderHour.getTime()));
            }
        },hour,min,true);
        timePickerDialog.show();
    }
    public void setCallback(Callback callback){
        this.callback = callback;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fullscreen_dialog_close:
                dismiss();
                break;
            case R.id.tvFromDate:
                setupDatePicker(tvFromDate,calendarDateFrom);
                break;
            case R.id.tvFromTime:
                setupTimePicker(tvFromTime,calenderHourFrom);
                break;
            case R.id.tvEndDate:
                setupDatePicker(tvEndDate,calendarDateEnd);
                break;
            case R.id.tvEndTime:
                setupTimePicker(tvEndTime,calenderHourEnd);
                break;
            default:
                if(tvTitle.getText().toString().equalsIgnoreCase("")){
                    Toast.makeText(getContext(),"Không được để trống tên sự kiện",Toast.LENGTH_SHORT).show();
                }
                else{
                    int year = calendarDateFrom.get(Calendar.YEAR);
                    int month = calendarDateFrom.get(Calendar.MONTH);
                    int dayOfMonth = calendarDateFrom.get(Calendar.DAY_OF_MONTH);
//                    Toast.makeText(getContext(),dayOfMonth+"",Toast.LENGTH_SHORT).show();
                    int hour = calenderHourFrom.get(Calendar.HOUR_OF_DAY);
                    int min = calenderHourFrom.get(Calendar.MINUTE);
                    String location = tvLocation.getText().toString();
                    String nameEvent = tvTitle.getText().toString();
                    result.set(year,month,dayOfMonth,hour,min);
                    callback.onActionClick(result,calenderHourEnd,location,nameEvent);

                    dismiss();
                }

                break;
        }
    }
    Callback callback;
    public interface Callback{
        void onActionClick(Calendar calendarStart, Calendar calendarEnd, String location, String nameEvent);
    }
}
