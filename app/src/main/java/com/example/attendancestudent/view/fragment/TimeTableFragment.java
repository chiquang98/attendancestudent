package com.example.attendancestudent.view.fragment;

import android.app.Dialog;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.example.attendancestudent.R;
import com.example.attendancestudent.model.Event;
import com.example.attendancestudent.repository.firebase.EventDAO;
import com.example.attendancestudent.repository.firebase.FirebaseDAO;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimeTableFragment extends Fragment implements MonthLoader.MonthChangeListener, WeekView.EventClickListener, WeekView.EventLongPressListener {
    ActionMode mActionMode;
    WeekView mWeekView;
    private Dialog mDialogAddEvent;
    private ArrayList<WeekViewEvent> mNewEvents;
    private BottomSheetDialog bottomSheetDialog;
    private LinearLayout viewLinearLayout, editLinearLayout, deleteLinearLayout;

    public TimeTableFragment() {
        // Required empty public constructor
    }

    private void createBottomSheetDialog() {
        if (bottomSheetDialog == null) {
            ViewGroup parent;
            ViewGroup root;
            View view = LayoutInflater.from(getContext()).inflate(R.layout.bottom_sheet_time_table, null);
            viewLinearLayout = view.findViewById(R.id.viewEvent);
            editLinearLayout = view.findViewById(R.id.editEvent);
            deleteLinearLayout = view.findViewById(R.id.deleteEvent);
            bottomSheetDialog = new BottomSheetDialog(getContext());
            bottomSheetDialog.setContentView(view);

        }
    }

    public void showDialog() {
        bottomSheetDialog.show();
    }
    private List<Event> listEvent;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_time_table, container, false);
        mNewEvents = new ArrayList<WeekViewEvent>();
        initWeekView(view);
        actionBar();
        createBottomSheetDialog();
        listEvent = new ArrayList<>();
        loadEvents();
        return view;
    }
    void loadEvents(){

        FirebaseDAO.getInstance().getKeyTeacher(new FirebaseDAO.OnKeyTeacherListener() {
            @Override
            public void onReceiveKey(String keyTeacher) {

                EventDAO.getInstance().getAllEvent(keyTeacher, new EventDAO.OnEventListener() {
                    @Override
                    public void onEventChange(List<Event> eventList) {
                        listEvent = eventList;
                        ArrayList<WeekViewEvent> weekViewEventList = new ArrayList<>();
                        Log.e("CHECKLOADEVENT",eventList.size()+"");
                        int temp ;
                        int amountHour;
                        for(Event event:eventList){
                            Calendar startTime = Calendar.getInstance();
                            startTime.set(Calendar.DAY_OF_MONTH,event.getDay());
                            startTime.set(Calendar.HOUR_OF_DAY,event.getStartHour());
                            startTime.set(Calendar.MINUTE,event.getStartMinute());
                            startTime.set(Calendar.MONTH,event.getMonth());
                            startTime.set(Calendar.YEAR,event.getYear());
                            temp = event.getEndHour()-event.getStartHour();
                            amountHour = temp==0?1:temp;
                            Calendar endTime = (Calendar) startTime.clone();
                            endTime.add(Calendar.HOUR_OF_DAY,amountHour);
                            startTime.set(Calendar.MONTH,event.getMonth());
                            WeekViewEvent eventWeek = new WeekViewEvent(Long.parseLong(event.getIdWeekView()),event.getNameEvent(),event.getContentEvent(),startTime, endTime);
//                            eventWeek.setColor(getResources().getColor(R.color.event_color_01));
                            weekViewEventList.add(eventWeek);
                        }
                        mNewEvents = weekViewEventList;
                        mWeekView.notifyDatasetChanged();
                    }
                });
            }
        });
    }
    private void initWeekView(View view) {
        // Get a reference for the week view in the layout.
        mWeekView = view.findViewById(R.id.weekView);
        mWeekView.setNumberOfVisibleDays(5);
// Set an action when any event is clicked.
        mWeekView.setOnEventClickListener(this);
        mWeekView.setEventLongPressListener(this);
// The week view has infinite scrolling horizontally. We have to provide the events of a
// month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);

// Set long press listener for events.
//        mWeekView.setEventLongPressListener(mEventLongPressListener);
    }

    @Override
    public void onResume() {
        super.onResume();

//        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6,getActivity(),mLoaderCallback);
    }

    private void actionBar() {
        //check fragment
        Toolbar toolbar = getActivity().findViewById(R.id.topAppBar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        Fragment navHostFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        navHostFragment.getChildFragmentManager().getFragments().get(0);
        if (navHostFragment.getChildFragmentManager().getFragments().get(0) instanceof TimeTableFragment) {
            if (toolbar != null) {
                toolbar.setTitle("Thời gian biểu");
                Menu menu = toolbar.getMenu();
                if (menu.findItem(R.id.top_app_bar_menu_search).isVisible()) {
                    menu.findItem(R.id.top_app_bar_menu_search).setVisible(false);
                }
                if (!menu.findItem(R.id.top_app_bar_menu_add).isVisible()) {
                    menu.findItem(R.id.top_app_bar_menu_add).setVisible(true);
                }
                if (!menu.findItem(R.id.top_app_bar_menu_bookmark).isVisible()) {
                    menu.findItem(R.id.top_app_bar_menu_bookmark).setVisible(true);
                }
            }
        }
        handleEventToolbar(toolbar);
    }

    private void handleEventToolbar(Toolbar toolbar) {
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.top_app_bar_menu_add:
                        DialogFragment dialogFragment = AddEventFullScreenDialog.getInstance();
                        ((AddEventFullScreenDialog) dialogFragment).setCallback(new AddEventFullScreenDialog.Callback() {
                            @RequiresApi(api = Build.VERSION_CODES.O)
                            @Override
                            public void onActionClick(Calendar calendarStart, Calendar calendarEnd, String location, String nameEvent) {
                                int year = Calendar.getInstance().get(Calendar.YEAR);
                                int temp = calendarEnd.get(Calendar.HOUR) - calendarStart.get(Calendar.HOUR);
                                int amountHour = temp == 0 ? 1 : temp;
                                int day = calendarStart.get(Calendar.DAY_OF_MONTH);
                                int startHour =calendarStart.get(Calendar.HOUR);
                                int startMin = calendarStart.get(Calendar.MINUTE);
                                int month = calendarStart.get(Calendar.MONTH);
                                Calendar startTime = Calendar.getInstance();
                                startTime.set(Calendar.DAY_OF_MONTH, day);
                                startTime.set(Calendar.HOUR_OF_DAY,startHour );
                                startTime.set(Calendar.MINUTE,startMin );
                                startTime.set(Calendar.MONTH, month);
                                startTime.set(Calendar.YEAR, year);
                                Calendar endTime = (Calendar) startTime.clone();
                                endTime.add(Calendar.HOUR, amountHour);
                                endTime.set(Calendar.MONTH, month);
                                long idWeekView = Calendar.getInstance().getTimeInMillis();
                                String idWeekViewStr = Long.toString(idWeekView);
                                WeekViewEvent eventWeek = new WeekViewEvent(idWeekView, nameEvent,location, startTime, endTime);
                                eventWeek.setColor(getResources().getColor(R.color.event_color_01));
                                mNewEvents.add(eventWeek);
                                mWeekView.notifyDatasetChanged();
                                FirebaseDAO.getInstance().getKeyTeacher(new FirebaseDAO.OnKeyTeacherListener() {
                                    @Override
                                    public void onReceiveKey(String keyTeacher) {
                                        Event event = new Event(keyTeacher,nameEvent,location,day,month,year,startHour,startMin,calendarEnd.get(Calendar.HOUR),calendarEnd.get(Calendar.MINUTE),idWeekViewStr);
                                        EventDAO.getInstance().addEvent(event);
                                    }
                                });

                                Log.e("QUANG", "INN mNewEvents " + mNewEvents.size() + "|" + calendarStart.getTime().getDay());
                            }

                        });
                        dialogFragment.show(getActivity().getSupportFragmentManager(), "TAG");
                        break;
                    case R.id.viewDay:
                        mWeekView.setNumberOfVisibleDays(1);
                        break;
                    case R.id.view3Days:
                        mWeekView.setNumberOfVisibleDays(3);
                        break;
                    case R.id.viewAllDays:
                        mWeekView.setNumberOfVisibleDays(5);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
        ArrayList<WeekViewEvent> newEvents = getNewEvents(newYear, newMonth);
        events.addAll(newEvents);
        return events;
    }

    private ArrayList<WeekViewEvent> getNewEvents(int year, int month) {

        // Get the starting point and ending point of the given month. We need this to find the
        // events of the given month.
        Calendar startOfMonth = Calendar.getInstance();
        startOfMonth.set(Calendar.YEAR, year);
        startOfMonth.set(Calendar.MONTH, month - 1);
        startOfMonth.set(Calendar.DAY_OF_MONTH, 1);
        startOfMonth.set(Calendar.HOUR_OF_DAY, 0);
        startOfMonth.set(Calendar.MINUTE, 0);
        startOfMonth.set(Calendar.SECOND, 0);
        startOfMonth.set(Calendar.MILLISECOND, 0);
        Calendar endOfMonth = (Calendar) startOfMonth.clone();
        endOfMonth.set(Calendar.DAY_OF_MONTH, endOfMonth.getMaximum(Calendar.DAY_OF_MONTH));
        endOfMonth.set(Calendar.HOUR_OF_DAY, 23);
        endOfMonth.set(Calendar.MINUTE, 59);
        endOfMonth.set(Calendar.SECOND, 59);

        // Find the events that were added by tapping on empty view and that occurs in the given
        // time frame.
        ArrayList<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
        for (WeekViewEvent event : mNewEvents) {
            if (event.getEndTime().getTimeInMillis() > startOfMonth.getTimeInMillis() &&
                    event.getStartTime().getTimeInMillis() < endOfMonth.getTimeInMillis()) {
                events.add(event);
            }
        }
        return events;
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {

    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        showDialog();
        deleteLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNewEvents.remove(event);
                mWeekView.notifyDatasetChanged();
                bottomSheetDialog.dismiss();
                String keyEv = "";
                for(Event ev:listEvent){
                    if(ev.getIdWeekView().equalsIgnoreCase(Long.toString(event.getId()))){
                        keyEv = ev.getKeyEvent();
                        break;
                    }
                }
                EventDAO.getInstance().deleteEventByKeyEvent(keyEv);
            }
        });
        editLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = AddEventFullScreenDialog.getInstance();
                dialogFragment.show(getActivity().getSupportFragmentManager(), "TAG");
                bottomSheetDialog.dismiss();
            }
        });
        viewLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

//    private List<WeekViewEvent> getEvents(int year,int month){
//        return events;
//    }

}
