package com.example.attendancestudent.view.login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.attendancestudent.R;
import com.example.attendancestudent.repository.firebase.FirebaseDAO;
import com.example.attendancestudent.viewmodel.LoginViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    private Button btnLogin;
    private EditText edtUserName;
    private EditText edtPassWord;
    private TextView tvRegister, tvForgotPass;
    private NavController navController;
    private LoginViewModel loginViewModel;
    private final String TAG = LoginFragment.class.getSimpleName();
    public LoginFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(actionBar!=null){
            if(actionBar.isShowing()){
                actionBar.hide();
            }
        }
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);
        if (savedInstanceState != null) {
            System.out.println("recovered");
            edtUserName.setText(savedInstanceState.getString("username"));
            edtPassWord.setText(savedInstanceState.getString("password"));
        }
        Toolbar toolbar = getActivity().findViewById(R.id.topAppBar);
        toolbar.setVisibility(View.GONE);
        //init view widget
        initView(view);
        return view;
    }
    private void initView(View view) {
        edtUserName = view.findViewById(R.id.editTextEmail);
        edtPassWord = view.findViewById(R.id.editTextPassword);
        btnLogin = view.findViewById(R.id.cirLoginButton);
        tvRegister = view.findViewById(R.id.tvRegister);
        tvForgotPass = view.findViewById(R.id.tvForgotPass);
        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvForgotPass.setOnClickListener(this);
        BottomNavigationView navView = getActivity().findViewById(R.id.nav_view);
        navView.setVisibility(View.GONE);
        navView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_SELECTED);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        if(loginViewModel.onLoggin()){
            navController.navigate(R.id.action_loginFragment_to_bottom_nav_class3);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {//dung de luu trang thai cu
        super.onSaveInstanceState(outState);
        outState.putString("username", edtUserName.getText().toString());
        outState.putString("password", edtPassWord.getText().toString());
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cirLoginButton:
                System.out.println("ClickedLoginButton");
                final String email = edtUserName.getText().toString();
                final String password = edtPassWord.getText().toString();
                loginViewModel.login(email, password, new FirebaseDAO.OnLogginListener() {
                    @Override
                    public void onSuccessful() {
                        Log.e("LOGIN","SUCCESS LOGIN ");
                        navController.navigate(R.id.action_loginFragment_to_bottom_nav_class3);
                    }

                    @Override
                    public void onFalied() {
                        Toast.makeText(getContext(), "Wrong Pass", Toast.LENGTH_LONG).show();
                    }
                });
                loginViewModel.getState().observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if (aBoolean == false) {
                            Toast.makeText(getContext(), "Empty Input", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                break;
            case R.id.tvRegister:
                navController.navigate(R.id.action_loginFragment_to_registerFragment);
                break;
            case R.id.tvForgotPass:
                break;
        }
    }
}
