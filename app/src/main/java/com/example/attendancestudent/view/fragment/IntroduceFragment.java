package com.example.attendancestudent.view.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.airbnb.lottie.LottieAnimationView;
import com.example.attendancestudent.R;
import com.example.attendancestudent.repository.firebase.FirebaseDAO;
import com.example.attendancestudent.view.login.LoginFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroduceFragment extends Fragment {
    LottieAnimationView animationView;
    public IntroduceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_introduce, container, false);




//        ActionBar actionBar = getSupportActionBar();
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(actionBar!=null){
            actionBar.hide();
        }
//        actionBar();
        return view;
    }
//    void actionBar(){
//        androidx.appcompat.widget.Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.topAppBar);
//
//        toolbar.setVisibility(View.INVISIBLE);
//    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BottomNavigationView navView = getActivity().findViewById(R.id.nav_view);
        navView.setVisibility(View.GONE);
        NavController navController = Navigation.findNavController(view);
//
//        if(FirebaseDAO.getInstance().onLoggin()==true){
//            navController.navigate(R.id.action_introduceFragment_to_loginFragment);
//        }
        animationView = view.findViewById(R.id.introduce_animation);

        System.out.println(animationView.isAnimating());
        Handler handler =new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                  navController.navigate(R.id.action_introduceFragment_to_loginFragment);
//                fragmentTransaction.replace(R.id.fragment_login,loginFragment);
//                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//                fragmentTransaction.commit();
            }
        },2800);

    }

    @Override
    public void onStart() {
        super.onStart();
        animationView.playAnimation();
    }
}
