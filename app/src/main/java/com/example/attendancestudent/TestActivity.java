package com.example.attendancestudent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.attendancestudent.model.Attendee;
import com.example.attendancestudent.repository.firebase.AttendeeDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TestActivity extends AppCompatActivity {
    AttendeeDAO attendeeDAO;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        attendeeDAO = AttendeeDAO.getInstance();
        Button btn = findViewById(R.id.btnTest);
        Button btn1 = findViewById(R.id.btnTest1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attendeeDAO.getAttendeeByClass("-MByKRmgJax4gjf4se2P", new AttendeeDAO.OnAttendeesListener() {
                    @Override
                    public void onReceiveAttendees(Map<String, List<Attendee>> maps) {
//                        System.out.println(list.get(0).get("daytest3").get(0).getKeyStudent());
                        System.out.println(maps.get("daytest3").get(0).getKeyStudent());
                    }
                });
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String day = "daytest3";
                String keyClass = "-MDQ90lpfmsZzuBhj5_u";
                List<Attendee> attendees = new ArrayList<>();
                attendees.add(new Attendee(true,"abc"));
                attendees.add(new Attendee(false,"abd"));
//                attendees.add(new Attendee(false,"KeyStu3"));
//                attendees.add(new Attendee(true,"KeyStu4"));
//                attendees.add(new Attendee(false,"KeyStu5"));
                attendeeDAO.attend(attendees,day,keyClass);
            }
        });
    }
}