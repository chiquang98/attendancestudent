package com.example.attendancestudent.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.attendancestudent.listenerinterface.DataLoadListener;
import com.example.attendancestudent.model.Class;
import com.example.attendancestudent.repository.firebase.FirebaseDAO;

import java.util.ArrayList;
import java.util.List;

public class ClassViewModel extends ViewModel {

    public static String TAG = ClassViewModel.class.getSimpleName();
//    private ArrayList<Class> mListClass;
    private MutableLiveData<List<Class>> mListClass;
    private FirebaseDAO firebaseDAO;
    //    private String keyTeacher = "";
    private MutableLiveData<String> keyTeacher;

    public ClassViewModel() {
        firebaseDAO = FirebaseDAO.getInstance();
    }

    public void setKeyTeacher(String keyTea) {
        if (keyTeacher == null) {
            keyTeacher = new MutableLiveData<>();
            keyTeacher.postValue(keyTea);
        }
        else{
            keyTeacher.postValue(keyTea);
        }
//        mListClass = firebaseDAO.getAllClasses(keyTea, listener);
    }
    public void setListClass(List<Class> listClass){
        if(mListClass==null){
            mListClass = new MutableLiveData<>();
            mListClass.setValue(listClass);
        }
    }

    public LiveData<List<Class>> getClasses() {
        if(mListClass==null){
            mListClass = new MutableLiveData<>();
        }
        return mListClass;
    }

    public void deleteClass(int position, String key) {
        if (mListClass != null) {
            List<Class> currentClasses = mListClass.getValue();
            Log.d(TAG,"Size truoc khi Xoa: "+currentClasses.size());
            currentClasses.remove(position);
            Log.d(TAG,"Size sau khi Xoa: "+currentClasses.size());
//            mListClass.postValue(currentClasses);
            mListClass.setValue(currentClasses);
//            firebaseDAO.deleteClass(key);
        }
    }
//
    public void renameClass(int position, String newName) {
        if (mListClass != null) {
            List<Class> currentList = mListClass.getValue();
            Class temp = currentList.get(position);
            temp.setNameOfClass(newName);
            currentList.set(position, temp);
//            firebaseDAO.renameClass(temp.getKeyClass(), newName);
        }
    }

    //    public void getKeyTeacher(FirebaseDAO.OnKeyTeacherListener listener){
//        firebaseDAO.getKeyTeacher(listener);
//    }
    public LiveData<String> getKeyTeacher() {
        if (keyTeacher == null) {
            keyTeacher = new MutableLiveData<>();
        }
        return keyTeacher;
    }

//    public void addClass(String name, String subject) {
//        if (keyTeacher != "") {
//            Class tempClass = new Class(keyTeacher, name, subject);
//            mListClass.add(tempClass);
//            firebaseDAO.addClass(tempClass);
//        }
//    }
    public void addClass(Class classObj) {
        if (mListClass != null) {
            List<Class> currentClasses = mListClass.getValue();
            Log.d(TAG,"length Classes before:"+currentClasses.size());
            currentClasses.add(classObj);
            Log.d(TAG,"length Classes after:"+currentClasses.size());
            mListClass.setValue(currentClasses);
//            firebaseDAO.addClass(classObj);
        }
    }
}
