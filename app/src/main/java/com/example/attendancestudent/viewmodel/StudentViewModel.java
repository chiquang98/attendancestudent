package com.example.attendancestudent.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.attendancestudent.model.Attendee;
import com.example.attendancestudent.model.Student;
import com.example.attendancestudent.repository.firebase.AttendeeDAO;
import com.example.attendancestudent.repository.firebase.StudentDAO;

import java.util.List;

public class StudentViewModel extends ViewModel {
    private MutableLiveData<List<Student>> students;
    private MutableLiveData<String> keyTeacher;

    private StudentDAO studentDAO;

    public StudentViewModel() {
        studentDAO = StudentDAO.getInstance();
    }
    public void setKeyTeacher(String keyTea) {
        if (keyTeacher == null) {
            keyTeacher = new MutableLiveData<>();
            keyTeacher.postValue(keyTea);
        }
        else{
            keyTeacher.postValue(keyTea);
        }
//        mListClass = firebaseDAO.getAllClasses(keyTea, listener);
    }
    public LiveData<String> getKeyTeacher() {
        if (keyTeacher == null) {
            keyTeacher = new MutableLiveData<>();
        }
        return keyTeacher;
    }

    public LiveData<List<Student>> getStudents(){
        if(students==null){
            students = new MutableLiveData<>();
        }
        return students;
    }
    public void setStudents(List<Student> studentList){
        if(students==null){
            students = new MutableLiveData<>();
            students.postValue(studentList);
        }
        else{
            students.postValue(studentList);
        }
    }
    public void addStudent(Student student,List<String> days,int numberOfStu){
        if(students!=null){
            List<Student> currentStudents = students.getValue();
            currentStudents.add(student);
            students.postValue(currentStudents);
            AttendeeDAO.getInstance().addStutoAttendTable(new Attendee(false,student.getMsv()),days,student.getKeyClass(),numberOfStu);
            studentDAO.addStudent(student);
        }
    }
}
