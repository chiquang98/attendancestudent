package com.example.attendancestudent.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.attendancestudent.model.User;
import com.example.attendancestudent.repository.firebase.FirebaseDAO;


public class LoginViewModel extends ViewModel {
    private MutableLiveData<Boolean> state;
    private FirebaseDAO firebaseDAO;
    public LoginViewModel() {
        state = new MutableLiveData<>();
        state.setValue(true);
        firebaseDAO = FirebaseDAO.getInstance();
    }
    public MutableLiveData<Boolean> getState() {
        return state;
    }
    public void login(String email, String password, FirebaseDAO.OnLogginListener listener){
        if(email.compareToIgnoreCase("")==0||password.compareToIgnoreCase("")==0){
            state.setValue(false);
        }
        else{

            state.setValue(true);
            firebaseDAO.login(email,password,listener);
        }
    }
    public boolean onLoggin(){
       return firebaseDAO.onLoggin();
    }
}
