package com.example.attendancestudent.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.example.attendancestudent.repository.firebase.FirebaseDAO;

public class RegisterViewModel {
    private MutableLiveData<Boolean> state;
    private FirebaseDAO firebaseDAO;

    public RegisterViewModel() {
        state = new MutableLiveData<>();
        state.setValue(true);
        firebaseDAO = FirebaseDAO.getInstance();
    }
    public MutableLiveData<Boolean> getState() {
        return state;
    }
//    public void register()
}
