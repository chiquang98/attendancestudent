package com.example.attendancestudent.listenerinterface;

public interface OnBackPressedListener {
    void onBackPressed();
}
