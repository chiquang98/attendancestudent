package com.example.attendancestudent.listenerinterface;

public interface DialogDataListener {
    void onReceiveData(String className);
}
