package com.example.attendancestudent.listenerinterface;

import com.example.attendancestudent.model.Class;

import java.util.ArrayList;

public interface IGetListClass {
    void getListClass(ArrayList<Class> list);
}
