package com.example.attendancestudent.listenerinterface;

public interface IGetKeyOfTeacher {
    void takeKey(String key);
}
