package com.example.attendancestudent.listenerinterface;

import com.example.attendancestudent.model.Class;

import java.util.List;

public interface DataLoadListener {
//    public void onListLoaded(List<Class> classes);

    void onListLoaded(List<Class> listClass);
}
