package com.example.attendancestudent.listenerinterface;

public interface OnItemClickListener {
    void onItemClicked(int position,String keyClass);
}
