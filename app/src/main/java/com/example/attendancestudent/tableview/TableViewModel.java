/*
 * Copyright (c) 2018. Evren Coşkun
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.example.attendancestudent.tableview;

import androidx.annotation.NonNull;

import com.example.attendancestudent.tableview.model.Cell;
import com.example.attendancestudent.tableview.model.ColumnHeader;
import com.example.attendancestudent.tableview.model.RowHeader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evrencoskun on 4.02.2018.
 */

public class TableViewModel {


    // Columns indexes
    public static final int MOOD_COLUMN_INDEX = 3;
    public static final int GENDER_COLUMN_INDEX = 4;
    // Constant size for dummy data sets
    private static final int COLUMN_SIZE = 200;
    public static final String TAG = TableViewModel.class.getSimpleName();
//    // Constant values for icons
//    public static final int SAD = 1;
//    public static final int HAPPY = 2;
//    public static final int BOY = 1;
//    public static final int GIRL = 2;
    private static final int ROW_SIZE = 200;


//    // Drawables
//    @DrawableRes
//    private final int mBoyDrawable;
//    @DrawableRes
//    private final int mGirlDrawable;
//    @DrawableRes
//    private final int mHappyDrawable;
//    @DrawableRes
//    private final int mSadDrawable;

    public TableViewModel() {
    }





    @NonNull
    private List<RowHeader> getSimpleRowHeaderList() {

        List<RowHeader> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
//            Student student = listStu.get(i);
//            System.out.println(student.getFullName());
            RowHeader header = new RowHeader(String.valueOf(i),"" );
            list.add(header);
        }
        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    @NonNull
    private List<ColumnHeader> getRandomColumnHeaderList() {

        List<ColumnHeader> list = new ArrayList<>();
        for (int i = 0; i < COLUMN_SIZE; i++) {
            String title = "column " + i;
//                title = "large column " + i;
                title = "";
            ColumnHeader header = new ColumnHeader(String.valueOf(i), title);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    @NonNull
    private List<List<Cell>> getCellListForSortingTest() {
        /*
        sort 900 records theo studentid
        string studentid = record[0].studentid;
        new List<cell> listCell; // moi danh sach tuong ung 1 student, 1 phan tu tuong ung 1 ngay
        * for 900 records{
            string currentstudentid = record[i].studentid;
            if (currentstudentid != studentid){
                listTotal.add(listCell);
                listCell = new ArrayList<>();
                student = currentstudent;
            }
            new Cell();
            listCell.add(cell);
        * }
        *
        * */
        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            List<Cell> cellList = new ArrayList<>();
            for (int j = 0; j < COLUMN_SIZE; j++) {
                Object text = "";
//                Object text = "Tran Chi Quang " + j + " " + i;
                // Create dummy id.
                String id = j + "-" + i;
                Cell cell;
                cell = new Cell(id, text);
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

//    @DrawableRes
//    public int getDrawable(int value, boolean isGender) {
//        if (isGender) {
//            return value == BOY ? mBoyDrawable : mGirlDrawable;
//        } else {
//            return value == SAD ? mSadDrawable : mHappyDrawable;
//        }
//    }

    @NonNull
    public List<List<Cell>> getCellList() {
        return getCellListForSortingTest();
    }

    @NonNull
    public List<RowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    @NonNull
    public List<ColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }
}
