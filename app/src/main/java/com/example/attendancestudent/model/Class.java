package com.example.attendancestudent.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Class {
    private String idTea;
    private String nameOfClass;
    private String subject;
    private String keyClass;

    public Class() {
    }

    public String getKeyClass() {
        return keyClass;
    }

    public void setKeyClass(String keyClass) {
        this.keyClass = keyClass;
    }

    public Class(String idTea, String nameOfClass,String subject) {
        this.subject=subject;
        this.idTea = idTea;
        this.nameOfClass = nameOfClass;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getIdTea() {
        return idTea;
    }

    public void setIdTea(String idTea) {
        this.idTea = idTea;
    }

    public String getNameOfClass() {
        return nameOfClass;
    }

    public void setNameOfClass(String nameOfClass) {
        this.nameOfClass = nameOfClass;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("nameOfClass", nameOfClass);
        return result;
    }
}
