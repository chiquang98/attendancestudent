package com.example.attendancestudent.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Teacher {
    private String email;
    private String iduser;
    private String fullName;
    private String mgv;
    private String phoneNumber;

    public Teacher() {
    }

    public Teacher(String email, String iduser, String fullName, String mgv, String phoneNumber) {
        this.email = email;
        this.iduser = iduser;
        this.fullName = fullName;
        this.mgv = mgv;
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMgv() {
        return mgv;
    }

    public void setMgv(String mgv) {
        this.mgv = mgv;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
