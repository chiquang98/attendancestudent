package com.example.attendancestudent.model;
import com.google.firebase.database.IgnoreExtraProperties;
@IgnoreExtraProperties
public class Event {
    String keyEvent,keyTeacher;
    String nameEvent,contentEvent;
    int day,month,year;
    int startHour,startMinute,endHour,endMinute;
    String idWeekView;
    public Event() {
    }

    public String getKeyTeacher() {
        return keyTeacher;
    }

    public void setKeyTeacher(String keyTeacher) {
        this.keyTeacher = keyTeacher;
    }

    public String getIdWeekView() {
        return idWeekView;
    }

    public void setIdWeekView(String idWeekView) {
        this.idWeekView = idWeekView;
    }

    public String getKeyEvent() {
        return keyEvent;
    }

    public void setKeyEvent(String keyEvent) {
        this.keyEvent = keyEvent;
    }


    public Event(String keyTeacher, String nameEvent, String contentEvent, int day, int month, int year, int startHour, int startMinute, int endHour, int endMinute, String idWeekView) {
        this.keyTeacher = keyTeacher;
        this.nameEvent = nameEvent;
        this.contentEvent = contentEvent;
        this.day = day;
        this.month = month;
        this.year = year;
        this.startHour = startHour;
        this.startMinute = startMinute;
        this.endHour = endHour;
        this.endMinute = endMinute;
        this.idWeekView = idWeekView;
    }

    public String getNameEvent() {
        return nameEvent;
    }

    public void setNameEvent(String nameEvent) {
        this.nameEvent = nameEvent;
    }

    public String getContentEvent() {
        return contentEvent;
    }

    public void setContentEvent(String contentEvent) {
        this.contentEvent = contentEvent;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(int startMinute) {
        this.startMinute = startMinute;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(int endMinute) {
        this.endMinute = endMinute;
    }
}
