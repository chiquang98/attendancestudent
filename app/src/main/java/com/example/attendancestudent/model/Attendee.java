package com.example.attendancestudent.model;

public class Attendee {
    boolean attended;
    String keyStudent;
    String msv;
    public Attendee() {
    }

    public String getKeyStudent() {
        return keyStudent;
    }

    public void setKeyStudent(String keyStudent) {
        this.keyStudent = keyStudent;
    }

    public String getMsv() {
        return msv;
    }

    public void setMsv(String msv) {
        this.msv = msv;
    }

//    public Attendee(boolean attended, String keyStudent) {
//        this.attended = attended;
//        this.keyStudent = keyStudent;
//    }

    public Attendee(boolean attended, String msv) {
        this.attended = attended;
        this.msv = msv;
    }

    public Attendee(boolean attended, String keyStudent, String msv) {
        this.attended = attended;
        this.keyStudent = keyStudent;
        this.msv = msv;
    }

    public Attendee(boolean attended) {
        this.attended = attended;
    }

    public boolean isAttended() {
        return attended;
    }

    public void setAttended(boolean attended) {
        this.attended = attended;
    }
}
