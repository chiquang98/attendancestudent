package com.example.attendancestudent.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Student {
    private String email;
    private String keyClass;
    private String fullName;
    private String msv;
    private String phoneNumber;
    private String keyStudent;

    public String getKeyStudent() {
        return keyStudent;
    }

    public void setKeyStudent(String keyStudent) {
        this.keyStudent = keyStudent;
    }

    public Student(String email, String keyClass, String fullName, String msv, String phoneNumber) {
        this.email = email;
        this.keyClass = keyClass;
        this.fullName = fullName;
        this.msv = msv;
        this.phoneNumber = phoneNumber;
    }

    public Student() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKeyClass() {
        return keyClass;
    }

    public void setKeyClass(String keyClass) {
        this.keyClass = keyClass;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMsv() {
        return msv;
    }

    public void setMsv(String msv) {
        this.msv = msv;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
