package com.example.attendancestudent.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Attendance {

    private String IDAttend;
    private String IDStu;
    private String IDSubj;
    private String IDClass;
    private long date;
//    private String date;
    private String reason;
    private boolean status;

    public Attendance() {
    }

    public String getIDAttend() {
        return IDAttend;
    }

    public void setIDAttend(String IDAttend) {
        this.IDAttend = IDAttend;
    }

    public String getIDStu() {
        return IDStu;
    }

    public void setIDStu(String IDStu) {
        this.IDStu = IDStu;
    }

    public String getIDSubj() {
        return IDSubj;
    }

    public void setIDSubj(String IDSubj) {
        this.IDSubj = IDSubj;
    }

    public String getIDClass() {
        return IDClass;
    }

    public void setIDClass(String IDClass) {
        this.IDClass = IDClass;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Attendance(String IDStu, String IDSubj, String IDClass, long date, String reason, boolean status) {
        this.IDStu = IDStu;
        this.IDSubj = IDSubj;
        this.IDClass = IDClass;
        this.date = date;
        this.reason = reason;
        this.status = status;
    }
}
